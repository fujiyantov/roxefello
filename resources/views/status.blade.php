<table cellpadding="5" cellspacing="0" border="1">
	<tr>
		<td>SKU</td>
		<td>QTY</td>
		<td>REMAINING</td>
		<td>SISA SHARE</td>
	</tr>
	@foreach($item as $data)
		<tr>
			<td>{{ $data->sku }}</td>
			<td>{{ $data->stock($data->id) }}</td>
			<td>{{ $data->remaining($data->id) }}</td>
			<td>{{ $data->remainingShare($data->id) }}</td>
		</tr>
	@endforeach
</table>