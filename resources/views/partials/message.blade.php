@if (session('error') || session('success'))
	<div class="alert alert-{{ (session('error')? 'warning' : 'info' ) }} alert-dismissible fade show">
	    <strong>{{ (session('error')? 'Something wrong' : 'Success' ) }}!</strong> {{ (session('error')? session('error') : session('success') ) }}.

	    <!-- Order Number Telah Terdaftar -->
	    @if(Session::has('values'))
	    	<br><br>
	    	<strong>Sudah Terdaftar</strong><br>
	    	@foreach(Session::get('values') as $data)
	    		- {{ $data }}<br>
	    	@endforeach
	    	<?php Session::forget('values'); ?>
	    @endif
		
		<!-- Stock Habis -->

	    @if(Session::has('values_stock_kosong'))
	    	<br><br>
	    	<strong>Stock Item Yang Kosong, Segera Cek Kembali</strong><br>
	    	@foreach(Session::get('values_stock_kosong') as $data)
	    		- {{ $data }}<br>
	    	@endforeach
	    	<?php Session::forget('values_stock_kosong'); ?>
	    @endif

	    <!-- Item Tidak Terdafatar -->
	    @if(Session::has('values_item_kosong'))
	    	<br><br>
	    	<strong>Item Tidak Terdaftar atau Ada penaman yang tidak sesuai dengan sistem. Mohon Segera Perbaiki</strong><br>
	    	@foreach(Session::get('values_item_kosong') as $data)
	    		- {{ $data }}<br>
	    	@endforeach
	    	<?php Session::forget('values_item_kosong'); ?>
	    @endif
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	    </button>
	</div>
@endif