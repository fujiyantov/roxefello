<!-- Main Sidebar Container -->
<aside class="main-sidebar elevation-4 sidebar-light-info">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
        <img src="{{ asset('assets/logo2.png') }}" alt="RoxefelloLogo" width="150" class="ml-2">
        {{-- <span class="brand-text font-weight-light">Data Stock</span> --}}
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('assets/dist/img/user3-128x128.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <?php $waktu = rand('111', '999'); ?>
                <a href="{{ route('staff:edit', [substr(base64_encode(substr(md5(Auth::user()->id), 0, 10)), 0, 10).'9'.substr(base64_encode($waktu), 0, 4) , slug(Auth::user()->name)]) }}" {{ Session::put('key'.'9', base64_encode(substr(md5(Auth::user()->id), 0, 10))) }} class="d-block">{{ Auth::user()->name }} </a>

                {{-- <a href="{{ route('staff:edit', [base64_encode(substr(md5(Auth::user()->id), 0, 10)), slug(Auth::user()->name)]) }}" class="d-block">{{ Auth::user()->name }}</a> --}}
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview {{ setOpen('home*') }}">
                    <a href="#" class="nav-link {{ setActive('home*') }}">
                        <i class="nav-icon fa fa-dashboard"></i>
                        <p>
                            Dashboard
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('home') }}" class="nav-link {{ setActive('home*') }}">
                                <i class="fa fa-angle-right nav-icon"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('sales:index') }}" class="nav-link {{ setActive('sales*') }}">
                        <i class="nav-icon fa fa-folder-o"></i>
                        <p>
                            Sales
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview {{ setOpen(['item*', 'brand*', 'category*']) }}">
                    <a href="#" class="nav-link {{ setActive(['item*', 'brand*', 'category*']) }}">
                        <i class="nav-icon fa fa-folder-o"></i>
                        <p>
                            Merchandise
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('item:index') }}" class="nav-link {{ setActive('item') }}">
                                <i class="fa fa-angle-right nav-icon"></i>
                                <p>Item</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('brand:index') }}" class="nav-link {{ setActive('brand') }}">
                                <i class="fa fa-angle-right nav-icon"></i>
                                <p>Brand</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('category:index') }}" class="nav-link {{ setActive('category') }}">
                                <i class="fa fa-angle-right nav-icon"></i>
                                <p>Category</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @if( Auth::user()->role == 1 )
                    <li class="nav-item has-treeview {{ setOpen(['sold*', 'summary*']) }}">
                        <a href="#" class="nav-link {{ setActive(['sold*', 'summary*']) }}">
                            <i class="nav-icon fa fa-folder-o"></i>
                            <p>
                                Report
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('sold:index') }}" class="nav-link {{ setActive('sold') }}">
                                    <i class="fa fa-angle-right nav-icon"></i>
                                    <p>Qty Sold</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('summary:index') }}" class="nav-link {{ setActive('summary') }}">
                                    <i class="fa fa-angle-right nav-icon"></i>
                                    <p>Summary</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                
                    <li class="nav-item">
                        <a href="{{ route('staff:index') }}" class="nav-link {{ setActive('staff') }}">
                            <i class="nav-icon fa fa-folder-o"></i>
                            <p>
                                Staff
                            </p>
                        </a>
                    </li>
                    {{-- <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-folder-o"></i>
                            <p>
                                Log
                            </p>
                        </a>
                    </li> --}}
                    
                    <li class="nav-item" style="border-top: 1px solid #dee2e6">
                       <a href="{{ route('staff:backup') }}" class="nav-link {{ setActive('staff') }}">
                            <i class="nav-icon fa fa-database"></i>
                            <p>
                                Backup
                            </p>
                        </a>
                    </li>

                    <li class="nav-item" style="border-top: 1px solid #dee2e6">
                       <a href="#" class="nav-link {{ setActive('staff') }}" data-toggle="modal" data-target="#restore">
                            <i class="nav-icon fa fa-clock-o"></i>
                            <p>
                                Restore
                            </p>
                        </a>
                    </li>
                @endif
                <li class="nav-item" style="border-top: 1px solid #dee2e6">
                    <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="nav-icon fa fa-power-off"></i>
                        <p>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            Sign Out
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
