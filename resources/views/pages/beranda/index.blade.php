@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                {{-- <h1 class="m-0 text-dark">Dashboard</h1> --}}
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg mb-3 text-right">
                <select name="filter" id="filter" style="min-width: 150px;">
                    <option value="1">Today</option>
                    <option value="0">Summary</option>
                </select>
            </div>
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3 class="txt-sd">{{ number_format($sales_detail) }}</h3>
                        <p>IDR</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-cash"></i>
                    </div>
                    <a href="{{ route('sales:index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-default">
                    <div class="inner">
                        <h3 class="txt-s">{{ number_format($sales) }}</h3>

                        <p>Sales</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('sales:index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-default">
                    <div class="inner">
                        <h3 class="txt-i">{{ number_format($item) }}</h3>

                        <p>Merchandise</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-cube"></i>
                    </div>
                    <a href="{{ route('item:index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-default">
                    <div class="inner">
                        <h3 class="txt-b">{{ number_format($brand) }}</h3>

                        <p>Brands</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{ route('brand:index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <!-- ./col -->
        <!-- ./col -->
        </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@stop
@section('script')
<script>
    $(function () {
        $('#filter').on('change', function(){
            //alert(this.value);
            $.ajax({
                type: 'POST',
                url : '{{ route('dashboard:summary') }}',
                data: {
                    _token   : '{!!csrf_token()!!}',
                    _filter  : this.value
                },
                beforeSend: function(response){

                },
                success: function(response, textStatus, xhr){
                    console.log(xhr.status);

                    if (xhr.status == 200) {
                        $('.txt-sd').html(response.sales_detail);
                        $('.txt-i').html(response.item);
                        $('.txt-s').html(response.sales);
                        $('.txt-b').html(response.brand);
                        $('.txt-x').html(response.brand);
                    }
                },
                error: function(response, textStatus, xhr){
                    console.log(xhr.textStatus);
                }
            })
        })
    });
</script>
@stop