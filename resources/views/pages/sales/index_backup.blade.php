@extends('layouts.index')

@section('content')
<style>
.badge{
    font-weight: 400;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Sales</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-info">
                    <div class="card-header" style="border-top-left-radius: 0px; border-top-right-radius: 0px; padding: .3rem 1.25rem;">
                        <p>
                            <i class="fa fa-glass"></i> Click here for Filter & Export
                        </p>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Type</p>
                                    <select name="type" id="type" class="form-control select2 float-right">
                                        <option value="1">Shopee</option>
                                        <option value="2">Roxefello</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Order</p>
                                    <input type="text" name="order_number" value="{{ old('order_number') }}" class="form-control{{ $errors->has('order_number') ? ' is-invalid' : '' }} float-right" placeholder="Order Number">
                                    @if ($errors->has('order_number'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('order_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Date:</p>
                                    <input type="text" name="start_at" value="{{ old('start_at') }}" class="form-control{{ $errors->has('start_at') ? ' is-invalid' : '' }} float-right" placeholder="Start at">
                                    @if ($errors->has('start_at'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('start_at') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Date</p>
                                    <input type="text" name="end_at" value="{{ old('end_at') }}" class="form-control{{ $errors->has('end_at') ? ' is-invalid' : '' }} float-right" placeholder="End at">
                                    @if ($errors->has('end_at'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('end_at') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Receiver</p>
                                    <input type="text" name="customer_name" value="{{ old('customer_name') }}" class="form-control{{ $errors->has('customer_name') ? ' is-invalid' : '' }} float-right" placeholder="Customer name">
                                    @if ($errors->has('customer_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('customer_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Tlp</p>
                                    <input type="text" name="customer_contact" value="{{ old('customer_contact') }}" class="form-control{{ $errors->has('customer_contact') ? ' is-invalid' : '' }} float-right" placeholder="Customer contact">
                                    @if ($errors->has('customer_contact'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('customer_contact') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Print</p>
                                    <select name="type" id="type" class="form-control select2 float-right">
                                        <option value="0">All</option>
                                        <option value="1">Print</option>
                                        <option value="2">Done</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Action</p>
                                    <a href="#" class="btn btn-info"><i class="fa fa-search"></i> Filter</a>
                                    <a href="#" class="btn btn-warning"><i class="fa fa-print"></i> Print</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header mb-4">
                        <h4>
                            <i class="fa fa-cubes"></i> Index <span class="text-upppercase text-secondary weight-bold">Sales.</span>
                        </h4>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <form action="{{ route('sales:excel') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input name="excel" type="file" required style="width: 200px;">
                                    <button type="submit" class="btn btn-sm btn-secondary mr-2"><i class="fa fa-upload"></i> Upload</button>    
                                </form>
                                <a href="{{ route('sales:create') }}" class="btn btn-sm btn-default">Create Order</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <form action="{{ route('sales:print') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body table-responsive p-3 mb-4">
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <td style="width: 5%">#</td>
                                        <td style="width: 5%">Type</td>
                                        <td>Order Number</td>
                                        <td>Customer Name</td>
                                        <td>Created By</td>
                                        <td style="width: 130px">Option</td>
                                        <td style="width: 80px"><button class="btn btn-sm btn-secondary print"><i class="fa fa-print"></i> Print</button></td>
                                    </tr>  
                                </thead>
                                <tbody>
                                    @foreach($sales as $data)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>
                                            @if( $data->order_type == 1)
                                            <p class="badge badge-info p-1" style="color: #fff;">Shopee</p>
                                            @else
                                            <a class="badge badge-secondary p-1" style="color: #fff">Roxefello</a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="#" class="text-secondary">{{ $data->order_number }}</a><br>
                                            <span class=" text-sm">Order date: {{ $data->order_date }}</span>
                                        </td>
                                        <td>{{ $data->customer_name }}</td>
                                        <td>{{ $data->createdBy->name }}</td>
                                        <td>
                                            <a href="{{ route('sales:show', [$data->id, slug($data->order_number)]) }}" class="btn btn-sm btn-info"><i class="fa fa-folder"></i> Show</a>
                                            <a href="{{ route('sales:edit', [$data->id, slug($data->order_number)]) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i> Edit</a>
                                        </td>
                                        <td align="center">
                                            <input type="checkbox" name="print[]" value="{{ $data->id }}">
                                            @if($data->is_print == 1)  
                                                <span class="badge badge-secondary">close</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div><!-- /.row -->
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->l
@stop
@section('script')

@stop