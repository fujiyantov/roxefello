@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- Title -->
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">salaes</a></li>
                    <li class="breadcrumb-item active">detail</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Main content -->
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                <i class="fa fa-cubes"></i> Data Stock, Inc.
                                <small class="float-right text-sm">Date: {{ $sales->created_at->format('d/m/Y') }}</small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            From:
                            <address>
                                <strong>{{ $sales->createdBy->name }}, Inc.</strong><br>
                                Roxefello<br>
                                Original Makeup & Skincare<br>
                            </address>
                        </div>
                        <!-- /.col -->

                        <div class="col-sm-4 invoice-col">
                            To:
                            <address>
                                <strong>{{ $sales->customer_name }}</strong><br>
                                {!! $sales->customer_address !!}<br>
                                Phone: {{ $sales->customer_contact }}<br>
                            </address>
                        </div>

                        <div class="col-sm-4 invoice-col">
                            <br>
                            <b>Order ID:</b> {{ $sales->order_number }}<br>
                            <b>Order Date:</b> {{ $sales->order_date }}<br>
                            <b>Shipment:</b> 
                            @if( $sales->shipment == 1 ) 
                                GOJEK 
                            @elseif($sales->shipment == 2) 
                                JNE 
                            @elseif($sales->shipment == 3) 
                                SICEPAT 
                            @else
                                JNE
                            @endif <br>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Qty</th>
                                        <th>Product</th>
                                        <th>SKU #</th>
                                        {{-- <th>Description</th> --}}
                                        <th>Ext. COGS</th>
                                        <th>Ext. Retail</th>
                                        <th>Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $grand_total = 0 ?>
                                    @foreach($item as $data)
                                    <tr>
                                        <td>{{ $data->qty }}</td>
                                        <td>{{ $data->item->name }}</td>
                                        <td><a href="#">{{ $data->item->sku }}</a></td>
                                        {{-- <td>{!! substr($data->item->description, 0, 50).'...' !!}</td> --}}
                                        <td>Rp {{ number_format($data->cogs, 2) }}</td>
                                        <td>Rp {{ number_format($data->retail, 2) }}</td>
                                        <td>Rp {{ number_format($data->total, 2) }}</td>
                                        <?php $grand_total += $data->total ?>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-6">
                            <p class="lead text-sm">Payment Methods: <strong><u>Transfer</u> <i class="fa fa-check"></i></strong></p>
                        </div>
                        <!-- /.col -->
                        <div class="col-6">
                            {{-- <p class="lead">Amount Due 2/22/2018</p> --}}

                            <div class="table-responsive">
                                <table class="table">
                                    {{-- <tr>
                                        <th style="width:70%">Tax (10%)</th>
                                        <td>Rp {{ number_format(rand('11111', '999999')) }}</td>
                                    </tr> --}}
                                    <tr>
                                        <th style="width:70%">Grand Total</th>
                                        <td>Rp {{ number_format($grand_total, 2) }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.invoice -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
