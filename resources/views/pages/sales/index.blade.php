@extends('layouts.index')

@section('content')
<style>
.badge{
    font-weight: 400;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Sales</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-info">
                    <div class="card-header" style="border-top-left-radius: 0px; border-top-right-radius: 0px; padding: .3rem 1.25rem;">
                        <p>
                            <i class="fa fa-glass"></i> Click here for Filter & Export
                        </p>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Type</p>
                                    <select name="type" id="type" class="form-control select2 float-right">
                                        <option value="">All Type</option>
                                        <option value="1">Shopee</option>
                                        <option value="2">Roxefello</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Order</p>
                                    <input type="text" name="order_number" id="order_number" value="{{ old('order_number') }}" class="form-control{{ $errors->has('order_number') ? ' is-invalid' : '' }} float-right" placeholder="Order Number">
                                    @if ($errors->has('order_number'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('order_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Date:</p>
                                    <input type="text" name="start_at" id="start_at" value="{{ old('start_at') }}" class="form-control{{ $errors->has('start_at') ? ' is-invalid' : '' }} float-right order_date" placeholder="Start at">
                                    @if ($errors->has('start_at'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('start_at') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Date</p>
                                    <input type="text" name="end_at" id="end_at" value="{{ old('end_at') }}" class="form-control{{ $errors->has('end_at') ? ' is-invalid' : '' }} float-right order_date" placeholder="End at">
                                    @if ($errors->has('end_at'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('end_at') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Receiver</p>
                                    <input type="text" name="customer_name" id="customer_name" value="{{ old('customer_name') }}" class="form-control{{ $errors->has('customer_name') ? ' is-invalid' : '' }} float-right" placeholder="Customer name">
                                    @if ($errors->has('customer_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('customer_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Tlp</p>
                                    <input type="text" name="customer_contact" id="customer_contact" value="{{ old('customer_contact') }}" class="form-control{{ $errors->has('customer_contact') ? ' is-invalid' : '' }} float-right" placeholder="Customer contact">
                                    @if ($errors->has('customer_contact'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('customer_contact') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Print</p>
                                    <select name="print" id="print" class="form-control select2 float-right">
                                        <option value="">All</option>
                                        <option value="0">Print</option>
                                        <option value="1">Done</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Action</p>
                                    <a href="#" class="btn btn-info filter"><i class="fa fa-search"></i> Filter</a>
                                    <a href="#" class="btn btn-warning print" target="_BLANK"><i class="fa fa-print"></i> Print</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header mb-4">
                        <div class="row mb-2">
                            <div class="col-md-12">
                                <h4>
                                    <i class="fa fa-cubes"></i> Index <span class="text-upppercase text-secondary weight-bold">Sales.</span>
                                </h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="card-tools">
                                        <div class="input-group input-group-sm">
                                            <form action="{{ route('sales:excel') }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <input name="excel" type="file" required style="width: 200px;">
                                                <button type="submit" class="btn btn-sm btn-danger mr-2"><i class="fa fa-upload"></i> Shopee</button>    
                                            </form>
                                            <a href="{{ route('sales:create') }}" class="btn btn-sm btn-default">Create Order</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card-tools">
                                        <div class="input-group input-group-sm">
                                            {{-- <form action="{{ route('sales:tokped') }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <input name="excel" type="file" required style="width: 200px;">
                                                <button type="submit" class="btn btn-sm btn-secondary mr-2"><i class="fa fa-upload"></i> Tokped</button>    
                                            </form> --}}
                                            {{-- <a href="{{ route('sales:create') }}" class="btn btn-sm btn-default">Create Order</a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <form action="{{ route('sales:print') }}" method="POST" id="print-submit" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body table-responsive fetch p-3 mb-4">
                            @include('pages.sales.filter')
                        </div>
                    </form>
                </div>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div><!-- /.row -->
</div><!-- /.container-fluid -->
</section>

<span class="res-modal-sales">
@include('pages.sales.modal')
</span>

<!-- /.content -->l
@stop
@section('script')
    <script type="text/javascript">
        $('.order_date').datepicker({
            format: "yyyy-mm-dd"
        });

        $('.filter').on('click', function(e){
            e.preventDefault();

            var type             = $('#type').val();
            var order_number     = $('#order_number').val();
            var start_at         = $('#start_at').val();
            var end_at           = $('#end_at').val();
            var customer_name    = $('#customer_name').val();
            var customer_contact = $('#customer_contact').val();
            var print            = $('#print').val();

            $.ajax({
                type: 'POST',
                url : '{{ route('sales:filter') }}',
                data:{
                    _token              : '{{ csrf_token() }}',
                    _type               : type,
                    _order_number       : order_number,
                    _start_at           : start_at,
                    _end_at             : end_at,
                    _customer_name      : customer_name,
                    _customer_contact   : customer_contact,
                    _print              : print
                },
                beforeSend:function(response){

                },
                success:function(response){
                    // console.log(response);
                    $('.fetch').html(response);
                },
                error:function(response){

                }
            })
        })

        $('.print').on('click', function(e){
            e.preventDefault();

            $('#print-submit').submit();
        })
    </script>
    <script> 
      function printAll()
      {
        if(document.getElementById('checkall').checked){
            //alert('a');
            $('.print-sales').prop('checked', true);
        }else{
            $('.print-sales').prop('checked', false);
        }
      }
    </script>
    <script>
        $('.check').on('click', function(e){
            e.preventDefault();

            var id      = $(this).attr('data-id');

            // alert(id);
            // return false;

            $.ajax({
                type: 'POST',
                url : '{{ route('sales:check') }}',
                data:{
                    _token  : '{{ csrf_token() }}',
                    _id     : id,
                },
                beforeSend:function(response){

                },
                success:function(response){

                    console.log(response);
                    $('.res-modal-sales').html(response);
                    $('#sales-modal-check').modal('show');
                },
                error:function(response){

                }
            })
        })
    </script>
@stop