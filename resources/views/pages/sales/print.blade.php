<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Data Stock | Print</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<style>
    body{
        font-family: 'Gill Sans' !important;
        font-size: 12px!important;
    }
</style>
<body onload="window.print();">
    <div class="wrapper">
        @foreach($sales as $data)
        <table border="1" style="min-width: 100%">
            <tr>
                <td><img src="{{ asset('assets/print.png') }}" alt="" width="75px"></td>
                <td style="width: 50%; font-size: 15px;" valign="top">
                    <div class="col-sm-12 nvoice-col">
                        <address>
                            <span>TO </span>: {{ $data->customer_name }}<br><br>
                            <span>MOBILE </span>: {{ $data->customer_contact }}<br><br>
                            <span>ADDRESS </span>: {!! $data->customer_address !!}<br><br>
                            <span>SHIPPING </span>: 
                            @if($data->shipment == 1)
                                GOJEK
                            @elseif($data->shipment == 2)
                                JNE
                            @elseif($data->shipment == 3)
                                SICEPAT
                            @else
                                JNE
                            @endif

                            @if($data->shipment_type == 1)
                                REG
                            @elseif($data->shipment_type == 2)
                                BEST/YES
                            @else
                                
                            @endif

                            <br>
                        </address>
                        {{-- <b>From:</b> Roxefello<br>
                        <b>Shipment:</b> @if( $data->shipment == 1 ) GOJEK @elseif($data->shipment == 2) SHOPEE @else JNE @endif<br> --}}
                    </div>
                </td>
                {{-- <td><img src="{{ asset('assets/print.png') }}" alt="" width="75px"></td> --}}
                <td style="width: 50%; font-size: 13px;" valign="top">
                    <div class="col-sm-12 nvoice-col">
                        ORDER : <br><br>
                        @foreach($data->detail as $data)
                        - {{ $data->item->name }} | Qty : {{ $data->qty }}<br>  
                        @endforeach
                    </div>
                </td>
            </tr>
        </table>
        <br>
        @endforeach
    </div>
</body>
</html>
