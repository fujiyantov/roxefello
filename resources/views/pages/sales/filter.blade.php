<table class="table">
    <thead>
        <tr>
            <td style="width: 5%">#</td>
            <td style="width: 5%">Type</td>
            <td>Order Number</td>
            <td>Customer Name</td>
            <td>Customer Contact</td>
            <td style="width: 200px">Option</td>
            <td align="center" style="width: 80px"><input type="checkbox" onclick="printAll();" id="checkall" name=""></td>
        </tr>  
    </thead>
    <tbody>
        @if(count($sales) > 0)
            @foreach($sales as $data)
            <tr>
                <td>{{ ++$i }}</td>
                <td>
                    @if( $data->order_type == 1)
                    <p class="badge badge-info p-1" style="color: #fff;">Shopee</p>
                    @else
                    <a class="badge badge-secondary p-1" style="color: #fff">Roxefello</a>
                    @endif
                </td>
                <td>
                    <a href="#" class="text-secondary">{{ $data->order_number }}</a><br>
                    <span class=" text-sm">Order date: {{ $data->order_date }}</span>
                </td>
                <td>{{ $data->customer_name }}</td>
                <td>{{ $data->customer_contact }}</td>
                <td>
                    <a href="{{ route('sales:show', [$data->id, slug($data->customer_name)]) }}" class="btn btn-sm btn-info mb-1"><i class="fa fa-folder"></i> Show</a>
                    @if(Auth::user()->role == 1 || Auth::user()->role == 2)
                        @if($data->is_accepted != 1)
                            <a href="{{ route('sales:edit', [$data->id, slug($data->customer_name)]) }}" class="btn btn-sm btn-info mb-1"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('sales:destroy', [$data->id]) }}" onClick="return confirm('Apakah anda yakin akan menghapus sales ini?')" class="btn btn-sm btn-danger mb-1"><i class="fa fa-trash"></i></a>
                        @endif
                        
                        @if($data->is_accepted == 1)
                            <a href="#" class="btn btn-sm btn-info mb-1" data-id={{ $data->id }}><i class="fa fa-check"></i> Accepted</a>
                        @else
                            <a href="#" class="btn btn-sm btn-warning mb-1 check" data-id={{ $data->id }}><i class="fa fa-eye"></i></a>
                        @endif
                    @endif
                </td>
                <td align="center">
                    <input type="checkbox" class="print-sales" name="print[]" value="{{ $data->id }}">
                    @if($data->is_print == 1)  
                        <span class="badge badge-secondary">done</span>
                    @endif
                </td>
            </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7" align="center">Record not found</td>
            </tr>
        @endif
    </tbody>
</table>
<div style="float: right">
    @if(!isset($no_page))
        {{ $sales->links() }}
    @endif
</div>
<script>
    $('.check').on('click', function(e){
        e.preventDefault();

        var id      = $(this).attr('data-id');

        // alert(id);
        // return false;

        $.ajax({
            type: 'POST',
            url : '{{ route('sales:check') }}',
            data:{
                _token  : '{{ csrf_token() }}',
                _id     : id,
            },
            beforeSend:function(response){

            },
            success:function(response){

                console.log(response);
                $('.res-modal-sales').html(response);
                $('#sales-modal-check').modal('show');
            },
            error:function(response){

            }
        })
    })
</script>