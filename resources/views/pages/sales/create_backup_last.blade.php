@extends('layouts.index')

@section('content')
<style>
.ck-editor__editable {
    min-height: 150px;
}

.wzd {
  display:none;   
}

.table.no-border{
    border-top: 0px solid #dee2e6 !important;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">sales</a></li>
                    <li class="breadcrumb-item active">create</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <div class="card card-default">
            <div class="card-header">
                <h4>
                    <i class="fa fa-cubes"></i> Create <span class="text-upppercase text-secondary weight-bold">Order.</span>
                </h4>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <form action="{{ route('sales:store') }}" method="POST" id="form-sales">
              @csrf
                <div class="card-body mb-4">
                    <div class="row">
                        <div class="col-lg wzd" id="wzd-from">
                           <div id="wizard">
                               <h2>Order</h2>
                               <section>
                                   <div class="row">
                                     <h5 class="ml-2">Order <span class="text-upppercase text-secondary weight-bold">Information.</span></h5>
                                   </div>
                                   <hr>
                                   <div class="row">
                                        <div class="col-lg-8">
                                           <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p>Number</p>
                                                    <input type="text" name="order_number" id="orderNumber" class="form-control{{ $errors->has('order_number') ? ' is-invalid' : '' }} float-right mb-1" placeholder="Number" value="{{ $trx_code }}" required readonly>
                                                    @if ($errors->has('order_number'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('order_number') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="z-index: 1000">
                                                <div class="form-group">
                                                    <input type="text" name="order_date" class="form-control{{ $errors->has('order_date') ? ' is-invalid' : '' }} float-right mb-1" id="order_date" placeholder="yyyy-mm-dd" value="{{ old('order_date') }}"required>
                                                    @if ($errors->has('order_date'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('order_date') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>  
                                           </div>
                                           <div class="row">
                                           
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <p>Shipment</p>
                                                      <select name="shipment" id="" class="form-control">
                                                         <option value="1">GOJEK</option>
                                                         <option value="2">JNE</option>
                                                         <option value="3">SICEPAT</option>
                                                       </select> 
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                       <p>Type</p>
                                                       <select name="shipment_type" id="" class="form-control">
                                                         <option value="1">Yes/Reg</option>
                                                         <option value="2">Best/Reg</option>
                                                       </select>  
                                                   </div>
                                                </div>
                                           
                                           </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center" style="margin-top: 15px">
                                                <i class="fa fa fa-sign-in" style="font-size: 180px;color: #148ea170 "></i>
                                            </div>
                                        </div>
                                   </div>
                               </section>

                               <h2>Item</h2>
                               <section>
                                   <div class="row">
                                     <h5 class="ml-2">Item <span class="text-upppercase text-secondary weight-bold">Information.</span></h5>
                                   </div>
                                   <hr>
                                   <div class="row">
                                       <div class="col-md-10">
                                           <div class="form-group">
                                               <select name="select_item" id="select_item" class="form-control select2 item" style="width: 100%">
                                                    @foreach($item as $data)
                                                        <option value="{{ $data->id }}" 
                                                          data-nitem="{{ $data->name }}"
                                                          data-ncolor="{{ $data->color }}"
                                                          data-nsku="{{ $data->sku }}"
                                                          data-nsize="{{ $data->size }}"
                                                          data-nweight="{{ $data->weight }}" 
                                                          data-nunit="{{ $data->unit }}"
                                                          data-nreamining="{{ $data->remaining($data->id) }}"
                                                          data-nqty="{{ $data->stock($data->id) }}">
                                                          <!-- Content -->
                                                          <div class="col-3">{{ $data->name }}</div>
                                                          {{-- <div class="col-3">\ <strong>Qty</strong>: {{ $data->stock($data->id) }}</div>
                                                          <div class="col-3">\{{ $data->weight }}{{ ($data->unit == 1) ? 'gr' : 'ml' }}</div>
                                                          <div class="col-3">\ <strong>Remaining</strong>: {{ $data->remaining($data->id) }}{{ ($data->unit == 1) ? 'gr' : 'ml' }}</div>
                                                          <div class="col-3">\ <strong>Color</strong>: {{ $data->color }}</div>
                                                          <div class="col-3">\ <strong>SKU</strong>: {{ $data->sku }}</div> --}}
                                                           
                                                        </option>
                                                    @endforeach
                                               </select>
                                           </div>
                                       </div>
                                       <div class="col-md-2">
                                            <div class="form-group">
                                                <a class="btn btn-info btn-sm btn-block add-item" style="color: #fff"><i class="fa fa-plus"></i> Filter</a>
                                            </div>
                                       </div>
                                       <div class="col-lg">
                                         <div class="col-9 table-responsive">
                                            <table class="table text-13">
                                                <tr>
                                                    <th class="no-border">Color</th>
                                                    <td class="no-border">:</td>
                                                    <td class="no-border" id="ncolor">-</td>

                                                    <th class="no-border">SKU</th>
                                                    <td class="no-border">:</td>
                                                    <td class="no-border" id="nsku">-</td>
                                                </tr>
                                                <tr>
                                                    <th class="no-border">Qty</th>
                                                    <td class="no-border">:</td>
                                                    <td class="no-border" id="nqty">-</td>

                                                    <th class="no-border">Size</th>
                                                    <td class="no-border">:</td>
                                                    <td class="no-border" id="nsize">-</td>
                                                </tr>
                                                <tr>
                                                    <th class="no-border">Weight</th>
                                                    <td class="no-border">:</td>
                                                    <td class="no-border" id="nweight">-</td>

                                                    <th class="no-border">Remaining</th>
                                                    <td class="no-border">:</td>
                                                    <td class="no-border" id="nremaining">-</td>
                                                </tr>
                                            </table>
                                        </div>
                                       </div>
                                    </div>
                                   <hr>
                                   <span class="fetch-item">

                                   </span>
                               </section>

                               <h2>Customer</h2>
                               <section>
                                  <div class="row">
                                     <h5 class="ml-2">Customer <span class="text-upppercase text-secondary weight-bold">Information.</span></h5>
                                   </div>
                                   <hr>
                                       <div class="row">
                                           <!-- Data Penerima -->
                                               <div class="col-md-4">
                                                   <div class="form-group">
                                                       <p>Customer </p>
                                                       <input type="text" name="customer_name" class="form-control{{ $errors->has('customer_name') ? ' is-invalid' : '' }} float-right" value="{{ old('customer_name') }}" required>
                                                       @if ($errors->has('customer_name'))
                                                           <span class="invalid-feedback" role="alert">
                                                               <strong>{{ $errors->first('customer_name') }}</strong>
                                                           </span>
                                                       @endif
                                                   </div>
                                               </div>
                                               <div class="col-md-4">
                                                   <div class="form-group">
                                                       <p>No. Contact </p>
                                                       <input type="input" name="customer_contact" class="form-control{{ $errors->has('customer_contact') ? ' is-invalid' : '' }} float-right" value="{{ old('customer_contact') }}" required>
                                                       @if ($errors->has('customer_contact'))
                                                           <span class="invalid-feedback" role="alert">
                                                               <strong>{{ $errors->first('customer_contact') }}</strong>
                                                           </span>
                                                       @endif
                                                   </div>
                                               </div>
                                               <div class="col-md-8">
                                                   <div class="form-group">
                                                       <p>Address </p>
                                                       <textarea name="customer_address" {{-- id="address_editor" --}} cols="20" rows="5" class="form-control{{ $errors->has('customer_address') ? ' is-invalid' : '' }}" value="{{ old('customer_address') }}" required></textarea>
                                                       @if ($errors->has('customer_address'))
                                                           <span class="invalid-feedback" role="alert">
                                                               <strong>{{ $errors->first('customer_address') }}</strong>
                                                           </span>
                                                       @endif
                                                   </div>
                                               </div>
                                               <div class="col-lg-4">
                                                   <div class="text-center" style="margin-top: 0px">
                                                       <i class="fa fa fa-users" style="font-size: 180px;color: #148ea170 "></i>
                                                   </div>
                                               </div>
                                           <!--/ Data Penerima -->
                                       </div>
                               </section>
                           </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </form>
            <!-- /.card-body -->
            {{-- <div class="card-footer text-left">
                <div class="btn btn-secondary">Create</i></div>
            </div> --}}
        </div>
        <!-- /.card -->
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@stop

@section('script')
<!-- Page script -->
<script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>

<script type="text/javascript">
    $(document).ready ( function () {
        $("#wizard").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "none"
        });

        $('#wzd-from').show();

        $("a[href$='#finish']").on("click", function(){
          //alert('hai');
          $("#form-sales").submit();
        })

        $('#order_date').datepicker({
            format: "yyyy-mm-dd"
        });
        // Generate
        $('#generate').change(function(){

            var val = $(this).val();

            if(val == 1){
                $('#orderNumber').val('');
                $('#orderNumber').prop('readonly', false);
            }else{
                var order = generateOrder();
                $('#orderNumber').val(order);
                $('#orderNumber').prop('readonly', true);
            }
        });

        function generateOrder(){
            var year    = {{ date('y') }}
            var month   = {{ date('m') }}
            var day     = {{ date('d') }}
            var time    = {{ date('his') }}

            return 'RF'+year+month+day+time;
        }

        //Initialize Select2 Elements
        $('.select2').select2();

        $('.add-item').on('click', function(){
            var item = $('.item').val();
            
            //var name = $("#select_item option:selected").text();
            var nitem = $("#select_item option:selected").attr('data-nitem');
            var nqty  = $("#select_item option:selected").attr('data-nqty');

            //detail item 
            var ncolor      = $("#select_item option:selected").attr('data-ncolor');
            var nsize       = $("#select_item option:selected").attr('data-nsize');
            var nweight     = $("#select_item option:selected").attr('data-nweight');
            var nunit       = $("#select_item option:selected").attr('data-nunit');
            var nremaining  = $("#select_item option:selected").attr('data-nreamining');
            var nsku        = $("#select_item option:selected").attr('data-nsku');

            if (nunit == 1) { nunit = 'gr' }else{ nunit = 'ml' }
            if (nsize == 1) { nsize = 'Full' }else{ nsize = 'Share' }

            $('#nqty').html(nqty);
            $('#ncolor').html(ncolor);
            $('#nsize').html(nsize);
            $('#nweight').html(nweight+''+nunit+'/item');
            $('#nunit').html(nunit);
            $('#nremaining').html(nremaining+''+nunit);
            $('#nsku').html(nsku);

            /*debug*/
            // alert(nqty);
            // return false;

            $('.fetch-item').append('\
                <div class="row mt-2 attr-'+item+'">\
                    <div class="col-lg-6">\
                        <div class="input-group">\
                            <div class="input-group-prepend">\
                                <span class="input-group-text package-'+item+' hide">\
                                    Package&nbsp;\
                                    <input type="checkbox" name="package" class="www-'+item+'" id="www-'+item+'" onclick="contoh4('+item+');">\
                                </span>\
                                <span class="input-group-text">\
                                    Item&nbsp;\
                                </span>\
                            </div>\
                            <input type="text" name="sales_item[]" value="'+nitem+'" class="form-control" readonly>\
                            <input type="hidden" name="item_id[]" value="'+item+'" class="form-control">\
                        </div>\
                    </div>\
                    <div class="col-lg-2 attrItem-'+item+'">\
                        <input type="text" name="sales_qty[]" class="form-control" placeholder="QTY">\
                    </div>\
                    <div class="col-lg-2 attrItem-'+item+' attr-retail-'+item+' hide">\
                        <input type="text" name="sales_retail[]" class="form-control" placeholder="Retail">\
                    </div>\
                    <div class="col-lg-2 attrItem-'+item+'">\
                       <button class="btn btn-warning btn-block" onclick="contoh3('+item+');">Delete</button>\
                    </div>\
                    <div class="col-lg-2 plus-'+item+' hide">\
                        <a class="btn btn-info plus-split" class="yyy" id="yyy" onclick="contoh2('+item+');" style="color: #fff"><i class="fa fa-plus"></i></a>\
                    </div>\
                </div>\
                <span class="split-item-'+item+'"></span>');
        });
    });
</script>
<script>
    ClassicEditor
        .create( document.querySelector( '#address_editor' ) )
        .then( editor => {
            height: 400;
            //console.log( editor );
        } )
        .catch( error => {
            //console.error( error );
        } );
</script>
<script> 
  function contoh($item)
  {
    if (document.getElementById('xxx-'+$item+'').checked) 
    {
      //alert($item)
        $('.plus-'+$item+'').removeClass('hide');
        $('.attrItem-'+$item+'').addClass('hide');
        $('.package-'+$item+'').addClass('hide');
    }else {
        $('.plus-'+$item+'').addClass('hide');
        $('.attrSplit-'+$item+'').remove();
        $('.attrItem-'+$item+'').removeClass('hide');
        $('.package-'+$item+'').removeClass('hide');
        $('.attr-retail-'+$item+'').addClass('hide');
    }
  }
</script>

<script> 
  function contoh4($item)
  {
    if (document.getElementById('www-'+$item+'').checked) 
    {
      //alert($item)
        $('.attr-retail-'+$item+'').removeClass('hide');
        $('.share-'+$item+'').addClass('hide');
    }else {
        $('.attr-retail-'+$item+'').addClass('hide');
        $('.share-'+$item+'').removeClass('hide');
    }
  }
</script>

<script> 
  function contoh2($item)
  {
    $('.split-item-'+$item+'').append('\
        <div class="row mt-2 ml-5 attrSplit-'+$item+'">\
             <div class="col-lg-3">\
                 <input type="text" name="share_weight_'+$item+'[]" class="form-control" placeholder="Weight">\
             </div>\
             <div class="col-lg-3">\
                 <input type="text" name="share_qty_'+$item+'[]" class="form-control" placeholder="QTY">\
             </div>\
        </div>');
  }
</script>

<script> 
  function contoh3($item)
  {
    $('.attr-'+$item+'').remove();
    $('.split-item-'+$item+'').remove();
  }
</script>
@stop
