@extends('layouts.index')

@section('content')
<style>
.ck-editor__editable {
    min-height: 150px;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">sales</a></li>
                    <li class="breadcrumb-item active">edit</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <div class="card card-default">
            <div class="card-header">
                <h4>
                    <i class="fa fa-cubes"></i> Edit <span class="text-upppercase text-secondary weight-bold">Order.</span>
                </h4>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <form action="{{ route('sales:update', [$sales->id, slug($sales->customer_name)]) }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Order Number </label>
                                        <input type="text" name="order_number" value="{{ $sales->order_number }}" id="orderNumber" class="form-control float-right" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Order Date</label>
                                        <input type="text" name="order_date" class="form-control float-right datepicker_normaly" value="{{ $sales->order_date  }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Order Type </label>
                                        <select name="order_type" class="form-control">
                                            <option value="1" @if($sales->order_type == 1) selected @endif>Shopee</option>
                                            <option value="2" @if($sales->order_type == 2) selected @endif>Roxefello</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Data Penerima -->
                                    <div class="form-group">
                                        <label>Customer </label>
                                        <input type="text" name="customer_name" class="form-control float-right" value="{{ $sales->customer_name }}">
                                    </div>
                                    <div class="form-group">
                                        <label>No. Contact </label>
                                        <input type="noTlp" name="customer_contact" class="form-control float-right" value="{{ $sales->customer_contact }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Shipment </label>
                                        <select name="shipment" id="" class="form-control">
                                            <option value="1" @if($sales->shipment == 1) selected @endif>GOJEK</option>
                                            <option value="2" @if($sales->shipment == 2) selected @endif>JNE</option>
                                            <option value="3" @if($sales->shipment == 3) selected @endif>SICEPAT</option>
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address </label>
                                        <textarea name="customer_address" cols="20" rows="5" id="addressck" class="form-control">{{ $sales->customer_address }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                {{-- <div class="col-md-12 mb-3">
                                    <hr>
                                    <span class="btn btn-info add-merchandise"> <i class="fa fa-plus"></i> Add Item</span>
                                </div> --}}
                                <div class="col-md-12">
                                    <table class="table table-hover table-bordered" style="font-size: 14px;">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th style="width: 400px;">SKU</th>
                                                <th>Qty</th>
                                                <th>COGS</th>
                                                <th>Retail</th>
                                                <th>Option</th>
                                            </tr>
                                        </thead>
                                        <tbody class="fetch-merchandise">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-secondary">Update </button>
                </div>
            </div>
        </form>
        <!-- /.card -->
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@stop

@section('script')
<!-- Page script -->
<script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#addresscka' ) )
        .then( editor => {
            height: 400;
            //console.log( editor );
        } )
        .catch( error => {
            //console.error( error );
        } );
</script>
<script>
  $(function () {

    //Initialize Select2 Elements
    $('.select2').select2()

    //Date range picker
    $('#order_date').datepicker()

    var no = 1;
        var item = <?php echo $item;?>;
        var detail = <?php echo $detail;?>;
        $.each(detail, function(i, value) {
            $('.fetch-merchandise').append('<tr class="'+no+'">\
                    <td align="center">'+no+'</td>\
                    <td>\
                        <select name="item[]" class="form-control select2 item_list_'+no+'" style="width: 100%;">\
                        </select>\
                    </td>\
                    <td>\
                        <input type="number" name="qty[]" id="qty" class="form-control qty_'+no+' readonly">\
                    </td>\
                    <td>\
                        <input type="number" name="cogs[]" id="rank_min_point" class="form-control cogs_'+no+'">\
                    </td>\
                    <td>\
                        <input type="number" name="retail[]" id="rank_min_point" class="form-control retail_'+no+'">\
                    </td>\
                    <td>\
                        <button class="btn btn-sm btn-default delete" data-id="'+no+'">Delete</button>\
                    </td>\
                </tr>');
            $.each(item, function(i, data) {
                    $('.item_list_'+no).append($('<option>').text(data.name).attr('value', data.id));
            });
            $('.item_list_'+no).val(value.sku);
            $('.qty_'+no).val(value.qty);
            $('.cogs_'+no).val(value.cogs);
            $('.retail_'+no).val(value.retail);
            init_delete();
            $('.select2').select2();
            no++;
        });

        $('.add-merchandise').click(function(){
            $('.fetch-merchandise').append('<tr class="'+no+'">\
                  <td align="center">'+no+'</td>\
                  <td>\
                      <select name="item[]" class="form-control select2 sku_list_'+no+'" style="width: 100%;">\
                      </select>\
                  </td>\
                  <td>\
                      <input type="number" name="qty[]" id="qty" class="form-control">\
                  </td>\
                  <td>\
                      <input type="number" name="cogs[]" id="rank_min_point" class="form-control">\
                  </td>\
                  <td>\
                      <input type="number" name="retail[]" id="rank_min_point" class="form-control">\
                  </td>\
                  <td>\
                      <button class="btn btn-sm btn-default delete" data-id="'+no+'">Delete</button>\
                  </td>\
              </tr>');
            parsing_sku(no);
            init_delete();
            $('.select2').select2();

            no++;
        });

        function parsing_sku(no)
        {
            $.each(item, function(i, value) {
                $('.sku_list_'+no).append($('<option>').text(value.name).attr('value', value.id));
            });
        }
        
        function init_delete()
        {
            $('.delete').click(function(){
                var id = $(this).attr('data-id');
                $('tr.'+id).remove();
               // alert('removed');
            });
        }
  })
</script>
@stop
