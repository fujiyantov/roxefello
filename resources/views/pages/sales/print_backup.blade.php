<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 3 | Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap 4 -->

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body onload="window.print();">
    <div class="wrapper">
        <!-- Main content -->
        
        @foreach($sales as $data)

            <div class="invoice p-3 mb-3">
                <!-- title row -->
                <div class="row">
                    <div class="col-12">
                        <h4>
                            <small class="float-right text-sm">Date: {{ $data->created_at->format('d/m/Y') }}</small>
                        </h4>
                    </div>
                    <!-- /.col -->
                </div>
                <p class="text-left"><img src="{{ asset('assets/logo2.png') }}" alt="RoxefelloLogo" class="elevation-2" width="150" style="margin-left:-10px;"></p>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        From:
                        <address>
                            <strong>{{ $data->createdBy->name }}, Inc.</strong><br>
                            Roxefello<br>
                            Original Makeup & Skincare<br>
                        </address>
                    </div>
                    <!-- /.col -->

                    <div class="col-sm-4 invoice-col">
                        To:
                        <address>
                            <strong>{{ $data->customer_name }}</strong><br>
                            {{ $data->customer_address }}<br>
                            Phone: {{ $data->customer_contact }}br>
                        </address>
                    </div>

                    <div class="col-sm-4 invoice-col">
                        <br>
                        <b>Order ID:</b> {{ $data->order_number }}<br>
                        <b>Order Date:</b> {{ $data->order_date}}<br>
                        <b>Shipment:</b> {{ $data->shipment }}<br>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Qty</th>
                                    <th>Product</th>
                                    <th>SKU #</th>
                                    <th>Ext. COGS</th>
                                    <th>Ext. Retail</th>
                                    <th>Sub Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $grand_total = 0; ?>
                                @foreach($data->detail as $data)
                                    <tr>
                                        <td>{{ $data->qty }}</td>
                                        <td>{{ $data->item->name }}</td>
                                        <td>{{ $data->item->sku }}</td>
                                        <td>Rp {{ number_format($data->cogs, 2) }}</td>
                                        <td>Rp {{ number_format($data->retail, 2) }}</td>
                                        <td>Rp {{ number_format($data->total, 2) }}</td>
                                        <?php $grand_total += $data->total ?>
                                    </tr>   
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-6">
                        <p class="lead text-sm">Payment Methods: <strong><u>Transfer</u> </strong></p>
                    </div>
                    <!-- /.col -->
                    <div class="col-6">
                        {{-- <p class="lead">Amount Due 2/22/2018</p> --}}

                        <div class="table-responsive">
                            <table class="table">
                                {{-- <tr>
                                    <th style="width:70%">Tax (10%)</th>
                                    <td>Rp {{ number_format(rand('11111', '999999')) }}</td>
                                </tr> --}}
                                <tr>
                                    <th style="width:62%">Total</th>
                                    <td>Rp {{ number_format($grand_total, 2) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <hr>
        @endforeach
        <!-- /.content -->
    </div>
    <!-- ./wrapper -->
</body>
</html>
