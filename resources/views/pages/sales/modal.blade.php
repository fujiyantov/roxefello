<div class="modal fade" id="sales-modal-check" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('sales:accepted') }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Check <span class="text-secondary weight-bold">Sales.</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="font-size: 14px;">
                    @if(isset($item))
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" id="" name="sales_id" value="{{ $sales->id }}">
                                <!-- info row -->
                                <div class="row invoice-info">

                                    <div class="col-sm-8 invoice-col">
                                        To:
                                        <address>
                                            <strong>{{ $sales->customer_name }}</strong><br>
                                            {!! $sales->customer_address !!}<br>
                                            Phone: {{ $sales->customer_contact }}<br>
                                        </address>
                                    </div>

                                    <div class="col-sm-4 invoice-col">
                                        <br>
                                        <b>Order ID:</b> {{ $sales->order_number }}<br>
                                        <b>Order Date:</b> {{ $sales->order_date }}<br>
                                        <b>Shipment:</b> 
                                        @if( $sales->shipment == 1 ) 
                                            GOJEK 
                                        @elseif($sales->shipment == 2) 
                                            JNE 
                                        @else 
                                            SICEPAT 
                                        @endif <br>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->

                                <!-- Table row -->
                                <div class="row">
                                    <div class="col-12 table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Qty</th>
                                                    <th>Product</th>
                                                    <th>SKU #</th>
                                                    <th>Ext. COGS</th>
                                                    <th>Ext. Retail</th>
                                                    <th>Sub Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $grand_total = 0 ?>
                                                @foreach($item as $data)
                                                <tr>
                                                    <td>{{ $data->qty }}</td>
                                                    <td>{{ $data->item->name }}</td>
                                                    <td><a href="#">{{ $data->item->sku }}</a></td>
                                                    <td>Rp {{ number_format($data->cogs, 2) }}</td>
                                                    <td>Rp {{ number_format($data->retail, 2) }}</td>
                                                    <td>Rp {{ number_format($data->total, 2) }}</td>
                                                    <?php $grand_total += $data->total ?>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-grand btn-item-submit">Accepted</button>
                </div>
            </form>
        </div>
    </div>
</div>