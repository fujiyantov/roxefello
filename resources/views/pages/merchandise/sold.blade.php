<table class="table" id="stock-sold">
    <tr>
        @for($i=1; $i<=$maxDays; $i++)
            <th style="min-width: 100px">{{ $i }}-{{ $month_name }}</th>
        @endfor
    </tr>
    <tr>
        <?php $arr = 0; ?>
        @for($i=1; $i<=$maxDays; $i++)
            <td>
                <?php 
                    $sales  = new App\SalesDetail;
                    echo $sales::sold($item->id, $month, $i); 
                ?>
            </td>
        @endfor
    </tr>
    <tr>
        <td colspan="1" align="left"><strong>Total</strong></td>
        <td colspan="{{ $i-2 }}" align="left"><strong>{{ $sold }}</strong></td>
    </tr>
</table>