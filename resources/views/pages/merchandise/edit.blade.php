@extends('layouts.index')

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">merchandise</a></li>
            <li class="breadcrumb-item active">edit</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- SELECT2 EXAMPLE -->
      <div class="card card-default">
        <div class="card-header">
          <h3 class="card-title">Edit Merchandise</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <form action="#">
                <div class="form-group">
                  <label>SKU </label>
                  <input type="text" name="order" class="form-control float-right" value="1A728b">
                </div>

                <div class="form-group">
                  <label>Name </label>
                  <input type="text" name="order" class="form-control float-right" value="Lorem Ipsum">
                </div>

                <div class="form-group">
                  <label>Brand </label>
                  <input type="text" name="order" class="form-control float-right" value="Lorem Ipsum">
                </div>

                <div class="form-group">
                  <label>Description </label>
                  <textarea name="" id="" cols="30" rows="10" class="form-control float-right">Lorem Ipsum Sit Dolor Amet</textarea>
                </div>

                <div class="form-group">
                  <label>Item </label>
                  <input type="text" name="order" class="form-control float-right" value="Lorem Ipsum">
                </div>

                <div class="form-group">
                  <label>Color </label>
                  <input type="text" name="order" class="form-control float-right" value="Lorem Ipsum">
                </div>

                <div class="form-group">
                  <label>Category </label>
                  <input type="text" name="order" class="form-control float-right" value="Lorem Ipsum">
                </div>

                <div class="form-group">
                  <label>Size </label>
                  <input type="text" name="order" class="form-control float-right" value="21m">
                </div>

                <div class="form-group">
                  <label>Weight </label>
                  <input type="text" name="order" class="form-control float-right" value="71'">
                </div>

                <div class="form-group">
                  <label>COGS </label>
                  <input type="number" name="order" class="form-control float-right" value="12000">
                </div>

                <div class="form-group">
                  <label>Price </label>
                  <input type="number" name="order" class="form-control float-right" value="120000">
                </div>

                <div class="form-group">
                  <label>Qty </label>
                  <input type="number" name="order" class="form-control float-right" value="100">
                </div>
              </form>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer text-right">
          <div class="btn btn-secondary">Update</i></div>
        </div>
      </div>
      <!-- /.card -->
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop
