@extends('layouts.index')

@section('content')
<style>
.ck-editor__editable {
    min-height: 150px;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">merchandise</a></li>
                    <li class="breadcrumb-item active">create</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Create Merchandise</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <form action="{{ route('item:store') }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Brand </label>
                                <select name="brand" id="brand" class="form-control select2 float-right">
                                    <option value="">Select...</option>
                                    @foreach($brand as $data)
                                        <option value="{{ $data->id }}">{{ $data->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>SKU </label>
                                <input type="text" name="sku" value="{{ old('sku') }}" class="form-control{{ $errors->has('sku') ? ' is-invalid' : '' }} float-right sku" readonly>
                                @if ($errors->has('sku'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sku') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{-- <div class="col-md-8">
                            <div class="form-group">
                                <label>Name </label>
                                <input type="text" name="name" value="{{ old('name') }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} float-right">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> --}}
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Description </label>
                                <textarea name="description" id="editor" cols="30" rows="5" value="{{ old('description') }}" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }} float-right"></textarea>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Item </label>
                                <input type="text" name="item" value="{{ old('item') }}" class="form-control{{ $errors->has('item') ? ' is-invalid' : '' }} float-right">
                                @if ($errors->has('item'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('item') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Color </label>
                                <input type="text" name="color" value="{{ old('color') }}" class="form-control{{ $errors->has('color') ? ' is-invalid' : '' }} float-right">
                                @if ($errors->has('color'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('color') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Category </label>
                                <select name="category" id="" class="form-control select2 float-right">
                                    <option value="">Select...</option>
                                    @foreach($category as $data)
                                        <option value="{{ $data->id }}">{{ $data->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Size </label>
                                {{-- <input type="text" name="size" value="{{ old('size') }}" class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }} float-right"> --}}
                                <select name="size" id="" class="form-control select2 float-right">
                                    <option value="1">Full</option>
                                    <option value="2">Share</option>
                                    
                                </select>
                                @if ($errors->has('size'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('size') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Weight </label>
                                <input type="text" name="weight" value="{{ old('weight') }}" class="form-control{{ $errors->has('weight') ? ' is-invalid' : '' }} float-right">
                                @if ($errors->has('weight'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>Unit </label>
                                <select name="unit" id="" class="form-control float-right">
                                    <option value="1">gr</option>
                                    <option value="2">ml</option>
                                </select>
                                @if ($errors->has('unit'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('unit') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-left">
                    <button type="submit" class="btn btn-secondary">Create</i></button>
                </div>
            </form>
        </div>
        <!-- /.card -->
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@stop
@section('script')
<!-- Page script -->
<script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        $('#brand').on('change', function(){
            // alert(this.value);
            $.ajax({
                type: 'POST',
                url : '{{ route('item:code') }}',
                data: {
                    _token  : '{!!csrf_token()!!}',
                    _brand  : this.value
                },
                beforeSend: function(response){

                },
                success: function(response, textStatus, xhr){
                    // console.log(xhr.status);
                    if (xhr.status == 200) {$('.sku').val(response);}
                },
                error: function(response, textStatus, xhr){
                    console.log(xhr.textStatus);
                    $('.sku').val('');
                }
            })
        })
    });
</script>
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
            height: 400;
            //console.log( editor );
        } )
        .catch( error => {
            //console.error( error );
        } );
</script>
@stop