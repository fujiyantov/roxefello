@extends('layouts.index')

@section('content')
<style>
.table.no-border{
    border-top: 0px solid #dee2e6 !important;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">merchandise</a></li>
                    <li class="breadcrumb-item active">detail</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <div class="row">
            <div class="col-12">
                <!-- Main content -->
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row mb-3">
                        <div class="col-12">
                            <h4>
                                <i class="fa fa-cubes"></i> Data <span class="text-upppercase text-secondary weight-bold">Item.</span>
                                <small class="float-right text-sm">Created at: {{ $item->created_at->format('Y-m-d H:i:s') }}</small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->

                    <!-- Table row -->
                    <div class="alert alert-info">
                        <div class="col-9 table-responsive">
                            <table class="table">
                                <tr>
                                    <td class="no-border">Name</td>
                                    <td class="no-border">:</td>
                                    <td class="no-border" width="250">{{ $item->name }}</td>

                                    <td class="no-border">SKU</td>
                                    <td class="no-border">:</td>
                                    <td class="no-border">{{ $item->sku }}</td>
                                </tr>
                                <tr>
                                    <td>Brand</td>
                                    <td>:</td>
                                    <td>{{ $item->brand->name }}</td>

                                    <td>Size</td>
                                    <td>:</td>
                                    <td>{{ ($item->size == 1)? 'Full' : 'Share' }}</td>
                                </tr>
                                <tr>
                                    <td>Category</td>
                                    <td>:</td>
                                    <td>{{ $item->category->name }}</td>

                                    <td>Weight</td>
                                    <td>:</td>
                                    <td>{{ $item->weight }}{{ ($item->unit == 'gr') ? 'gr' : 'ml' }} / Item</td>
                                </tr>
                                <tr>
                                    <td>Color</td>
                                    <td>:</td>
                                    <td>{{ ($item->color)? $item->color : '-' }}</td>

                                    <td>Status</td>
                                    <td>:</td>
                                    <td><span class="badge badge-warning">{{ ($item->is_published == 1)? 'Publish' : 'inpublish' }}</span></td>
                                </tr>
                                <tr>
                                    <td>Descripton</td>
                                    <td>:</td>
                                    <td colspan="4">
                                        {!! $item->description !!}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        @if(Auth::user()->role == 1 || Auth::user()->role == 2)
                            <div class="row">
                                <button class="btn btn-grand ml-4 mb-2 px-4 py-3" style="min-width: 200px;" data-toggle="modal" data-target="#pop-up"> Create Stock <i class="fa fa-angle-double-right"></i></button>

                                <button class="btn btn-grand ml-4 mb-2 px-4 py-3" style="min-width: 200px;" data-toggle="modal" data-target="#item-edit"> Edit Item <i class="fa fa-pencil"></i></button>
                            </div>
                        @endif
                    </div>
                    <!-- /.row -->
                    <div class="row mb-3 mt3">
                        <div class="card-body table-responsive p-3" id="sold">
                            @include('pages.merchandise.stock')
                        </div>
                    </div>
                </div>
                <!-- /.invoice -->

                <!-- Main content -->
                <div class="invoice p-3 mb-3">
                    <div class="row mb-3 mt-3">
                        <div class="col-12">
                            <h4>
                                <i class="fa fa-"></i> Item <span class="text-upppercase text-secondary weight-bold">Sold</span>.
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <div class="form-group">
                                <input type="text" class="datepicker_bulan_tahun form-control date-sold" name="tahun" value="{{ old('tahun') }}" placeholder="mm/yyyy">
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <button class="btn btn-info btn-block btn-filter"><i class="fa fa-search"></i> Filter</button>
                            </div>
                        </div>
                        <div class="col-12 table-responsive fetch-sold">
                            @include('pages.merchandise.sold')
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.invoice -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- Modal -->
<div class="modal fade" id="pop-up" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create <span class="text-secondary weight-bold">Stock.</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" id="id" class="id" value="0">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <p>Date </p>
                            <input type="text" name="date" id="date" value="{{ old('date') }}" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }} float-right date datepicker_normaly" required>
                            @if ($errors->has('date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6">
                        <div class="form-group">
                            <p>Qty </p>
                            <input type="text" name="qty" id="qty" value="{{ old('qty') }}" class="form-control{{ $errors->has('qty') ? ' is-invalid' : '' }} float-right mon-keyup qty" required>
                            @if ($errors->has('qty'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('qty') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 rx-remaining">
                        <div class="form-group">
                            <p>Remaining </p>
                            <input type="text" name="remaining" id="remaining" value="{{ old('remaining') }}" class="form-control{{ $errors->has('remaining') ? ' is-invalid' : '' }} float-right remaining" required>
                            @if ($errors->has('remaining'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('remaining') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <p>COGS </p>
                            <input type="text" name="cogs" id="cogs" value="{{ old('cogs') }}" class="form-control{{ $errors->has('cogs') ? ' is-invalid' : '' }} float-right mon-keyup cogs" placeholder="Rp." required>
                            @if ($errors->has('cogs'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('cogs') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <p>Retail </p>
                            <input type="text" name="retail" id="retail" value="{{ old('retail') }}" class="form-control{{ $errors->has('retail') ? ' is-invalid' : '' }} float-right mon-keyup retail" placeholder="Rp." required>
                            @if ($errors->has('retail'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('retail') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-grand btn-edit hide">Update</button> --}}
                <button type="button" class="btn btn-grand btn-submit">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="item-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('item:update', [$item->id, slug($item->name)]) }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Update <span class="text-secondary weight-bold">Item.</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="id" class="id" value="{{ $item->id }}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <p>Type Item </p>
                                <select name="type" id="type" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }} float-right type select2" style="width: 100%">
                                    <option value="1" @if($item->type == 1) selected @endif>Full</option>
                                    <option value="2" @if($item->type == 2) selected @endif>Share</option>
                                </select> 
                                @if ($errors->has('type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>

                            @if($item->type == 2 && $item->size == 2)
                                <div class="form-group">
                                    <p>Parent </p>
                                    <select name="parent" id="parent" class="form-control{{ $errors->has('parent') ? ' is-invalid' : '' }} float-right parent select2" style="width: 100%">
                                        @foreach($parent as $data)
                                            <option value="{{ $data->id }}" @if($data->id == $item->sub) selected @endif>{{ $data->name }}</option>
                                        @endforeach
                                    </select> 
                                    @if ($errors->has('parent'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('parent') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            @endif

                            <div class="form-group">
                                <p>Name </p>
                                <input type="text" name="name" id="name" value="{{ $item->name }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} float-right name">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <p>Category </p>
                                <select name="category" id="category" class="form-control{{ $errors->has('category') ? ' is-invalid' : '' }} float-right category select2">
                                    @foreach($category as $data)
                                        <option value="{{ $data->id }}" @if($data->id == $item->category_id) selected @endif>{{ $data->name }}</option>
                                    @endforeach
                                </select> 
                                @if ($errors->has('category'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <p>Brand </p>
                                <select name="brand" id="brand" class="form-control{{ $errors->has('brand') ? ' is-invalid' : '' }} float-right brand select2">
                                    @foreach($brand as $data)
                                        <option value="{{ $data->id }}" @if($data->id == $item->brand_id) selected @endif>{{ $data->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('brand'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('brand') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <p>Sku </p>
                                <input type="text" name="sku" id="sku" value="{{ $item->sku }}" class="form-control{{ $errors->has('sku') ? ' is-invalid' : '' }} float-right sku">
                                @if ($errors->has('sku'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sku') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <p>Size </p>
                                <select name="size" id="size" class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }} float-right size select2">
                                    <option value="1" @if($item->size == 1) selected @endif>Full</option>
                                    <option value="2" @if($item->size == 2) selected @endif>Share</option>
                                </select> 
                                @if ($errors->has('size'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('size') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <p>Weight </p>
                                <input type="text" name="weight" id="weight" value="{{ $item->weight }}" class="form-control{{ $errors->has('weight') ? ' is-invalid' : '' }} float-right weight">
                                @if ($errors->has('weight'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <p>Color </p>
                                <input type="text" name="color" id="color" value="{{ $item->color }}" class="form-control{{ $errors->has('color') ? ' is-invalid' : '' }} float-right color">
                                @if ($errors->has('color'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('color') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <p>Description </p>
                                <textarea name="description" id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }} float-right description" cols="30" rows="3">{{ $item->description }}</textarea>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-grand btn-item-submit">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.content -->
@stop
@section('script')
<script>
    $('.rx-remaining').hide();

    $(".mon-keyup").on('keyup', function(){
        var n = parseInt($(this).val().replace(/\D/g,''),10);
        $(this).val(n.toLocaleString());
    });


    $('.btn-submit').click(function(event) {
        /* Act on the event */
        var id      = $('.id').val();
        var date    = $('.date').val();
        var qty     = $('.qty').val();
        var cogs    = $('.cogs').val();
        var retail  = $('.retail').val();
        var remaining  = $('.remaining').val();
        // alert(remaining);
        // return false;
        $.ajax({
            type    :'POST',
            url     :'{{ route('stock:store') }}',
            data    :{
                _token   : '{!! csrf_token() !!}',
                _item    : {{ $item->id }},
                _id      : id,
                _date    : date,
                _qty     : qty,
                _cogs    : cogs,
                _retail  : retail,
                _remaining  : remaining,
            },
            beforeSend: function(response){

            },
            success: function(response){
                console.log(response);
                location.reload();
                // $('#pop-up').modal('hide');
                // $(".stock").load(" .stock");
                // $(".table").addClass('datatable');
            },
            error: function(response) {
                /* Act on the event */
            }
        });
    });

    $('.btn-filter').click(function(event) {
        /* Act on the event */
        var sold = $('.date-sold').val();

        $.ajax({
            type    :'POST',
            url     :'{{ route('stock:sold') }}',
            data    :{
                _token   : '{!! csrf_token() !!}',
                _item    : {{ $item->id }},
                _date    : sold,
            },
            beforeSend: function(response){

            },
            success: function(response){
                $('.fetch-sold').html(response);
            },
            error: function(response, textStatus, xhr){
                console.log(xhr.textStatus);
            }
        });
    });

    $('.btn-edit').click(function(event) {
        $('.rx-remaining').show();

        var id      = $(this).attr('data-id');
        var date    = $(this).attr('data-date');
        var qty     = $(this).attr('data-qty');
        var remaining  = $(this).attr('data-remaining');
        var cogs    = $(this).attr('data-cogs');
        var retail  = $(this).attr('data-retail');
        
        $('#id').val(id);
        $('#date').val(date);
        $('#qty').val(qty);
        $('#remaining').val(remaining);
        $('#cogs').val(cogs);
        $('#retail').val(retail);
        $('#pop-up').modal('show');
    }); 

    // $('.select2').select2(); 
</script>
@stop