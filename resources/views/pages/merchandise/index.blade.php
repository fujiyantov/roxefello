@extends('layouts.index')

@section('content')
<style>
    .input-group-text{
        background-color: #ffc107!important;
        border: #ffc107;
        color: #1f2d3d;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">merchandise</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
         <div class="row">
            <div class="col-lg-12">
                <div class="card card-info">
                    <div class="card-header" style="border-top-left-radius: 0px; border-top-right-radius: 0px; padding: .3rem 1.25rem;">
                        <p>
                            <i class="fa fa-glass"></i> Click here for Filter & Export
                        </p>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Type</p>
                                    <select name="type" id="type" class="form-control float-right">
                                        <option value="">All Type</option>
                                        <option value="1">Full Only</option>
                                        <option value="2">Share Only</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Name</p>
                                    <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} float-right" placeholder="Item name">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>SKU</p>
                                    <input type="text" name="sku" id="sku" value="{{ old('sku') }}" class="form-control{{ $errors->has('sku') ? ' is-invalid' : '' }} float-right order_date" placeholder="Item SKU">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Size</p>
                                    <select name="size" id="size" class="form-control float-right">
                                        <option value="">All Type</option>
                                        <option value="1">Full</option>
                                        <option value="2">Share</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Brand</p>
                                    <select name="brand" id="brand" class="form-control select2 float-right" style="width: 100%">
                                        <option value="">Select</option>
                                        @foreach($brand as $data)
                                            <option value="{{ $data->id }}">{{ $data->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-3">
                                    <p>Category</p>
                                    <select name="category" id="category" class="form-control select2 float-right" style="width: 100%">
                                        <option value="">Select</option>
                                        @foreach($category as $data)
                                            <option value="{{ $data->id }}">{{ $data->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 mt-3">
                                <p>Min</p>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text package">
                                            0
                                        </span>
                                    </div>
                                    <input type="text" name="min" id="min" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-2 mt-3">
                                <p>Max</p>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text package">
                                            999+
                                        </span>
                                    </div>
                                    <input type="text" name="max" id="max" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group mt-3">
                                    <p>Action</p>
                                    <a href="#" class="btn btn-info btn-block filter"><i class="fa fa-search"></i> Filter</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-header mb-4">
                        <h4>
                            <i class="fa fa-cubes"></i> Index <span class="text-upppercase text-secondary weight-bold">Item.</span>
                        </h4>

                        <div class="card-tools">
                            @if(Auth::user()->role == 1 || Auth::user()->role == 2)
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <a href="{{ route('item:create') }}" class="btn btn-sm btn-info btn-block">Create Item</a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-3 mb-4 fetch">
                        @include('pages.merchandise.filter')
                    </div>
                </div>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div><!-- /.row -->
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->l
@stop
@section('script')
    <script type="text/javascript">
        $('.select2').select2();

        $('.filter').on('click', function(e){
            e.preventDefault();

            var type     = $('#type').val();
            var name     = $('#name').val();
            var sku      = $('#sku').val();
            var size     = $('#size').val();
            var brand    = $('#brand').val();
            var category = $('#category').val();
            var max      = $('#max').val();
            var min      = $('#min').val();

            $.ajax({
                type: 'POST',
                url : '{{ route('item:filter') }}',
                data:{
                    _token       : '{{ csrf_token() }}',
                    _type        : type,
                    _name        : name,
                    _sku         : sku,
                    _size        : size,
                    _brand       : brand,
                    _category    : category,
                    _max         : max,
                    _min         : min
                },
                beforeSend:function(response){

                },
                success:function(response){
                    //console.log(response);
                    $('.fetch').html(response);
                },
                error:function(response){

                }
            })
        })
    </script>
@stop
