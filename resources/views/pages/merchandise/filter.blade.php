<table class="table">
    <thead>
        <tr>
            <td style="width: 5%">#</td>
            <td>Name</td>
            <td>SKU</td>
            <td>Size</td>
            <td>Weight</td>
            <td>Qty</td>
        </tr>
    </thead>
    <tbody>
        @if(count($item) > 0)
            @foreach($item as $data)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>
                        <a href="{{ route('item:show', [$data->id, slug($data->name)]) }}" class="{{ ($data->stock($data->id) < 1) ? 'text-danger' : 'text-black' }}" style=" font-weight: 700">{{ $data->name }}</a><br>
                        <span class="text-12">Created at: {{ $data->created_at }}</span>

                    </td>
                    <td>{{ $data->sku }}</td>
                    <td>{{ ($data->size == 1) ? 'Full' : 'Share' }}</td>
                    <td>{{ $data->weight }}{{ $data->unit }}/item</td>
                    <td>{{ $data->stock($data->id) }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8" align="center">no Data...</td>
            </tr>
        @endif
    </tbody>
</table>
<div style="float: right">
    @if(!isset($no_page))
        @if(count($item) > 0)
            {{ $item->links() }}
        @endif
    @endif
</div>