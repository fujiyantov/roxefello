@extends('layouts.index')

@section('content')
<style>
.ck-editor__editable {
    min-height: 150px;
}

.select2-selection__rendered {
    line-height: 32px !important;
}
.select2-container .select2-selection--single {
    height: 36px !important;
}
.select2-selection__arrow {
    height: 35px !important;
}

</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">merchandise</a></li>
                    <li class="breadcrumb-item active">create</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Create Merchandise</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <form action="{{ route('item:store') }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Type </label>
                                <select name="type" id="" class="form-control float-right type-of-size">
                                    <option value="0">Select</option>
                                    <option value="1">Full</option>
                                    <option value="2">Share</option>
                                    
                                </select>
                                @if ($errors->has('type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- SHARE -->
                        <div class="col-md-6 share hide">
                            <div class="form-group">
                                <label>Select Item </label>
                                <select name="share_item" id="for-share-only" class="form-control for-share-only float-right select2" style="width: 100%;">
                                    <option value="">select</option>
                                    @foreach($item as $data)
                                        <option value="{{ $data->id }}" 
                                          data-nitem="{{ $data->name }}"
                                          data-ncolor="{{ $data->color }}"
                                          data-nsku="{{ $data->sku }}"
                                          data-nsize="{{ $data->size }}"
                                          data-nweight="{{ $data->weight }}" 
                                          data-nunit="{{ $data->unit }}"
                                          data-nreamining="{{ $data->remaining($data->id) }}"
                                          data-nqty="{{ $data->stock($data->id) }}">

                                          <!--/. name item -->
                                          <div class="col-3">{{ $data->name }}</div>
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!--/.SHARE -->
                        <div class="col-md-2 counter-item-share hide">
                            <a class="btn btn-info btn-block add-item" data-share-item="data" style="color: #fff; margin-top: 31px!important"><i class="fa fa-plus"></i> Share item</a>
                        </div>
                    </div>
                    <hr>
                    <!-- Detail Item-->
                    <div class="row show-share-item hide">
                        <div class="col-lg">
                            <div class="alert alert-warning" style="background: #fff!important">
                                <div class="col-9 table-responsive">
                                    <table class="table text-13">
                                        <tr>
                                            <th class="no-border">Color</th>
                                            <td class="no-border">:</td>
                                            <td class="no-border" id="ncolor">-</td>

                                            <th class="no-border">SKU</th>
                                            <td class="no-border">:</td>
                                            <td class="no-border" id="nsku">-</td>
                                        </tr>
                                        <tr>
                                            <th class="no-border">Qty</th>
                                            <td class="no-border">:</td>
                                            <td class="no-border" id="nqty">-</td>

                                            <th class="no-border">Size</th>
                                            <td class="no-border">:</td>
                                            <td class="no-border" id="nsize">-</td>
                                        </tr>
                                        <tr>
                                            <th class="no-border">Weight</th>
                                            <td class="no-border">:</td>
                                            <td class="no-border" id="nweight">-</td>

                                            <th class="no-border">Remaining</th>
                                            <td class="no-border">:</td>
                                            <td class="no-border" id="nremaining">-</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-item-share hide">
                        <div class="col-12 table-responsive">
                            <table class="table text-13">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <td>Weight</td>
                                        {{-- <td>Cogs</td>
                                        <td>Retail</td> --}}
                                        <td>Option</td>
                                    </tr>
                                </thead>
                                <tbody class="fetch-item"></tbody>
                            </table>
                        </div>
                    </div>

                    <!--/.Detail Item-->                                            
                    <!-- FULL -->
                    <div class="row full hide">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-md-6 mb-1">
                                    <div class="form-group">
                                        <label>Brand </label>
                                        <select name="brand" id="brand" class="form-control float-right select2" style="width: 100%;">
                                            <option value="">Select...</option>
                                            @foreach($brand as $data)
                                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>SKU </label>
                                        <input type="text" name="sku" value="{{ old('sku') }}" class="form-control{{ $errors->has('sku') ? ' is-invalid' : '' }} float-right sku" readonly>
                                        @if ($errors->has('sku'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('sku') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Item </label>
                                        <input type="text" name="item" value="{{ old('item') }}" class="form-control{{ $errors->has('item') ? ' is-invalid' : '' }} float-right">
                                        @if ($errors->has('item'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('item') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description </label>
                                        <textarea name="description" id="editor" cols="30" rows="5" value="{{ old('description') }}" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }} float-right"></textarea>
                                        @if ($errors->has('description'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Size </label>
                                        <select name="size" id="share_size" class="form-control float-right">
                                            <option value="0">Select...</option>
                                            <option value="1">Full Only</option>
                                            <option value="2">Share Only</option>
                                        </select>
                                        @if ($errors->has('size'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('size') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Color </label>
                                        <input type="text" name="color" value="{{ old('color') }}" class="form-control{{ $errors->has('color') ? ' is-invalid' : '' }} float-right">
                                        @if ($errors->has('color'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('color') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Category </label>
                                        <select name="category" id="" class="form-control float-right">
                                            <option value="">Select...</option>
                                            @foreach($category as $data)
                                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label>Unit </label>
                                        <select name="unit" id="" class="form-control float-right">
                                            <option value="gr">gr</option>
                                            <option value="ml">ml</option>
                                        </select>
                                        @if ($errors->has('unit'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('unit') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                {{-- <div class="col-md-3 share_size_full hide"> --}}
                                <div class="col-md-3">
                                   <div class="form-group">
                                       <label>Weight </label>
                                       <input type="text" name="weight" value="{{ old('weight') }}" class="form-control{{ $errors->has('weight') ? ' is-invalid' : '' }} float-right">
                                       @if ($errors->has('weight'))
                                           <span class="invalid-feedback" role="alert">
                                               <strong>{{ $errors->first('weight') }}</strong>
                                           </span>
                                       @endif
                                   </div>
                               </div>
                            </div>  
                        </div>
                    </div>
                    <!--/.FULL -->
                    <div class="share_size_only hide mt-4">
                        <a class="btn btn-info add-share-only" style="color: #fff">Split</a>
                        <div class="col-12 table-responsive mt-4">
                            <table class="table text-13">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <td>Weight</td>
                                        <td>Option</td>
                                    </tr>
                                </thead>
                                <tbody class="fetch-item-share"></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-left">
                    <button type="submit" class="btn btn-secondary btn-change hide">Create</i></button>
                </div>
            </form>
        </div>
        <!-- /.card -->
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@stop
@section('script')
<!-- Page script -->
<script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        $('.type-of-size').on('change', function(){
            // alert(this.value);
            var type = this.value;
            if (type == 1) {
                $('.full').removeClass('hide');
                $('.share').addClass('hide');
                $('.btn-change').removeClass('hide');
                $('.show-share-item').addClass('hide');
                $('.counter-item-share').addClass('hide');
                $('.table-item-share').addClass('hide');
            }else if(type == 2){
                $('.full').addClass('hide');
                $('.share').removeClass('hide');
                $('.btn-change').removeClass('hide');
                $('.share_size_only').addClass('hide');
            }else{
                $('.full').addClass('hide');
                $('.share').addClass('hide');
                $('.btn-change').addClass('hide');
                $('.show-share-item').addClass('hide');
                $('.counter-item-share').addClass('hide');
                $('.table-item-share').addClass('hide');
                $('.share_size_only').addClass('hide');
            }
        })

        $('.for-share-only').on('change', function(){
            var nid   = this.value;
            $('.show-share-item').removeClass('hide');
            $('.table-item-share').removeClass('hide');
            $('.counter-item-share').removeClass('hide');
            $('.counter-item-share a').attr('data-share-item', nid);
            $('tr.remove-row').remove();

            var nitem = $("#for-share-only option:selected").attr('data-nitem');
            var nqty  = $("#for-share-only option:selected").attr('data-nqty');

            //detail item 
            var ncolor      = $("#for-share-only option:selected").attr('data-ncolor');
            var nsize       = $("#for-share-only option:selected").attr('data-nsize');
            var nweight     = $("#for-share-only option:selected").attr('data-nweight');
            var nunit       = $("#for-share-only option:selected").attr('data-nunit');
            var nremaining  = $("#for-share-only option:selected").attr('data-nreamining');
            var nsku        = $("#for-share-only option:selected").attr('data-nsku');

            if (nunit == 'gr') { nunit = 'gr' }else{ nunit = 'ml' }
            if (nsize == 'ml') { nsize = 'Full' }else{ nsize = 'Share' }

            $('#nqty').html(nqty);
            $('#ncolor').html(ncolor);
            $('#nsize').html(nsize);
            $('#nweight').html(nweight+''+nunit+'/item');
            $('#nunit').html(nunit);
            $('#nremaining').html(nremaining+''+nunit);
            $('#nsku').html(nsku);
        });

        var rec  = 1;
        $('.add-item').on('click', function(){
            var item = $(".counter-item-share a").attr('data-share-item');
            $('.fetch-item').append('\
                <tr class="item-'+rec+' remove-row">\
                    <td>'+rec+'</td>\
                    <td>\
                        <input type="text" name="share_weight[]" value="{{ old('share_weight') }}" class="form-control{{ $errors->has('share_weight') ? ' is-invalid' : '' }} float-right">\
                        @if ($errors->has('share_weight'))\
                            <span class="invalid-feedback" role="alert">\
                                <strong>{{ $errors->first('share_weight') }}</strong>\
                            </span>\
                        @endif\
                    </td>\
                    <td>\
                        <span class="btn btn-warning btn-block delete-item-share" data-delete-item="item-'+rec+'">Delete</span>\
                    </td>\
                </tr>\
            ');

            delete_item_share();
            rec++;
        });

        function delete_item_share(){
            $('.delete-item-share').click(function(){
                var id = $(this).attr('data-delete-item');
                $('tr.'+id).remove();
            });
        }

        $('#share_size').on('change', function(){
            var val = this.value;
            if (val == 1) {
                $('.share_size_full').removeClass('hide');
                $('.share_size_only').addClass('hide');
            }else if(val == 2){
                $('.share_size_full').addClass('hide');
                $('.share_size_only').removeClass('hide');
            }else{
                $('.share_size_full').addClass('hide');
                $('.share_size_only').addClass('hide');
            }
        });

        var num  = 1;
        $('.add-share-only').on('click', function(){
            $('.fetch-item-share').append('\
                <tr class="item-only-'+num+' remove-row-only">\
                    <td>'+num+'</td>\
                    <td>\
                        <input type="text" name="share_weight_only[]" value="{{ old('share_weight_only') }}" class="form-control{{ $errors->has('share_weight_only') ? ' is-invalid' : '' }} float-right">\
                        @if ($errors->has('share_weight_only'))\
                            <span class="invalid-feedback" role="alert">\
                                <strong>{{ $errors->first('share_weight_only') }}</strong>\
                            </span>\
                        @endif\
                    </td>\
                    <td>\
                        <span class="btn btn-warning btn-block delete-item-share-only" data-delete-item-only="item-only-'+num+'">Delete</span>\
                    </td>\
                </tr>\
            ');

            delete_item_share_only();
            num++;
        });

        function delete_item_share_only(){
            $('.delete-item-share-only').click(function(){
                var id = $(this).attr('data-delete-item-only');
                $('tr.'+id).remove();
            });
        }

        $('#brand').on('change', function(){
            // alert(this.value);
            $.ajax({
                type: 'POST',
                url : '{{ route('item:code') }}',
                data: {
                    _token  : '{!!csrf_token()!!}',
                    _brand  : this.value
                },
                beforeSend: function(response){

                },
                success: function(response, textStatus, xhr){
                    // console.log(xhr.status);
                    if (xhr.status == 200) {$('.sku').val(response);}
                },
                error: function(response, textStatus, xhr){
                    console.log(xhr.textStatus);
                    $('.sku').val('');
                }
            })
        })
    });
</script>
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
            height: 400;
            //console.log( editor );
        } )
        .catch( error => {
            //console.error( error );
        } );
</script>
@stop