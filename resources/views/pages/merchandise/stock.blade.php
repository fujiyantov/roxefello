<table class="table stock" data-page-length='1'>
    <thead>
        <tr>
            <th style="width: 5%">#</th>
            <th>Purchase date</th>
            <th>Qty</th>
            <th>Total</th>
            <th>Share</th>
            <th>Cogs</th>
            <th>Retail</th>
            <th>Created By</th>
            @if(Auth::user()->role == 1 || Auth::user()->role == 2)
                <th>Option</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach($stock as $data)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $data->purchase_date->format('d-M-Y') }}</td>
                <td>{{ number_format($data->qty) }}</td>
                <td>{{ number_format($data->remaining, 2) }}{{ ($item->unit == 1) ? 'gr' : 'ml' }}</td>
                
                <?php 
                    $total = $item->weight * $data->qty;
                    $share = $data->remaining - $total;
                ?>

                <td>{{ number_format($share, 2) }}{{ ($data->unit == 1) ? 'gr' : 'ml' }}</td>
                <td>Rp. {{ number_format($data->cogs) }}</td>
                <td>Rp. {{ number_format($data->retail) }}</td>
                <td>{{ $data->createdBy->name }}</td>
                @if(Auth::user()->role == 1 || Auth::user()->role == 2)
                    <td>
                        <a href="javascript:;" class="btn-edit" data-id="{{ $data->id }}" data-date="{{ $data->purchase_date->format('Y-m-d') }}" data-qty="{{ $data->qty }}" data-remaining="{{ $data->remaining }}" data-cogs="{{ $data->cogs }}" data-retail="{{ $data->retail }}"><span class="badge badge-info">Update</span></a>

                        <a href="{{ route('stock:destroy', [$data->id]) }}" onClick="return confirm('Apakah anda yakin akan menghapus item ini?')"><span class="badge badge-danger">Delete</span></a>
                    </td>
                @endif
            </tr>
        @endforeach
    </tbody>       
</table> 
<div style="float: right">
    @if(count($stock) > 0)
        {{ $stock->links() }}
    @endif
</div>