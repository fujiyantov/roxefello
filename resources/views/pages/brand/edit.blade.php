@extends('layouts.index')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">brand</a></li>
                    <li class="breadcrumb-item active">edit</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
            <div class="card-header">
                <h4>
                    Edit <span class="text-upppercase text-secondary weight-bold">Brand</span>.
                </h4>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <form action="{{ route('brand:update', [$brand->id, slug($brand->name)]) }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Brand Name </label>
                                <input type="text" name="name" value="{{ ($brand->name)? $brand->name : old('name') }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} float-right">
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Brand Code </label>
                                <input type="text" name="code" value="{{ ($brand->code)? $brand->code : old('code') }}" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }} float-right">
                                @if ($errors->has('code'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-left">
                    @if(Auth::user()->role == 1 || Auth::user()->role == 2)
                        <button type="submit" class="btn btn-secondary">Update</i></button>
                    @endif
                </div>
            </form>
        </div>
        <!-- /.card -->
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@stop
