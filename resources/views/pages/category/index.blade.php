@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">category</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-header mb-4">
                        <h4>
                            <i class="fa fa-cubes"></i> Index <span class="text-upppercase text-secondary weight-bold">Category.</span>
                        </h4>

                        <div class="card-tools">
                            @if(Auth::user()->role == 1 || Auth::user()->role == 2)
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <a href="{{ route('category:create') }}" class="btn btn-sm btn-info btn-block">Create Category</a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-3 mb-4">
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <td style="width: 5%"></td>
                                    <td>Name</td>
                                    {{-- <td>Last Active</td> --}}
                                    @if(Auth::user()->role == 1 || Auth::user()->role == 2)
                                        <td>Option</td>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($category as $data)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>
                                        <a href="{{ route('category:edit', [$data->id, slug($data->name)]) }}">{{ $data->name }}</a><br>
                                        {{-- <span class="text-sm">Created at: {{ $data->created_at }}</span><br>
                                        <span class="text-sm">by: {{ $data->createdBy->name }}</span> --}}

                                    </td>
                                    {{-- <td>
                                        @if($data->updated_by)
                                            <span class="text-sm"> Updated at: {{ $data->updated_at }} </span><br>
                                            <span class="text-sm"> by: {{ $data->updatedBy->name }} </span>
                                        @endif
                                    </td> --}}
                                    @if(Auth::user()->role == 1 || Auth::user()->role == 2)
                                        <td>
                                            @if($data->is_published == 1)
                                                <a href="{{ route('category:destroy', [$data->id, slug($data->name)]) }}" class="btn btn-link btn-sm"><span class="badge badge-secondary">Un-Publish <i class="fa fa-close"></i></span></a>
                                            @else
                                                <a href="{{ route('category:actived', [$data->id, slug($data->name)]) }}" class="btn btn-link btn-sm"><span class="badge badge-info">Publish <i class="fa fa-check"></i></span></a>
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div><!-- /.row -->
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->l
@endsection
