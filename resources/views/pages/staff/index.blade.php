@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Staff</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header mb-4">
                        <h4>
                            <i class="fa fa-cubes"></i> Index <span class="text-upppercase text-secondary weight-bold">Staff.</span>
                        </h4>
                        @if( Auth::user()->role == 1 )
                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <a href="{{ route('staff:create') }}" class="btn btn-sm btn-info btn-block">Create Staff</a>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-3 mb-4">
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <td style="width: 5%"></td>
                                    <td>Name</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($staff as $data)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    @if( Auth::user()->role == 1 )
                                        <td>
                                            <?php $waktu = rand('111', '999'); ?>
                                            <a href="{{ route('staff:edit', [substr(base64_encode(substr(md5($data->id), 0, 10)), 0, 10).$i.substr(base64_encode($waktu), 0, 4) , slug($data->name)]) }}" {{ Session::put('key'.$i, base64_encode(substr(md5($data->id), 0, 10))) }}>{{ $data->name }} </a><br>
                                        </td>
                                    @else
                                        <td>{{ $data->name }}</td>
                                    @endif
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
