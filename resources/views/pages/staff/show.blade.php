@extends('layouts.index')

@section('content')
<style>
.table.no-border{
    border-top: 0px solid #dee2e6 !important;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">staff</a></li>
                    <li class="breadcrumb-item active">detail</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-solid">
              <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="" alt="User profile picture">

                <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

                <p class="text-muted text-center">{{ Auth::user()->email }}</p>

                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <b>Penelitian</b> <a href="#"</a>
                  </li>
                  <li class="list-group-item">
                    <b>Publikasi</b> <a href=""></a>
                  </li>
                  <li class="list-group-item">
                    <b>PKM</b> <a href="#"></a>
                  </li>
                </ul>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title">Informasi Tambahan</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">

                <ul class="list-group">
                  <li class="list-group-item">
                    <b>Kegiatan BIRO P3KM</b> <a href="" class="pull-right"></a>
                  </li>
                </ul>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
                <li><a href="#settings" data-toggle="tab">Settings</a></li>
              </ul>
              <div class="tab-content">
                <div class="active tab-pane" id="activity">
                  <div class="box-group" id="accordion">
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    
                  </div>

                </div>

                <div class="tab-pane" id="settings">
                  <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label for="inputName" class="col-sm-2 control-label">Nama</label>

                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" id="inputName" value="{{ Auth::user()->name }}" readonly>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                      <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" id="inputEmail" value="{{ Auth::user()->email }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputName" class="col-sm-2 control-label">Password</label>

                      <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" id="inputPassword">
                        <p class="text-warning">*kosongkan jika tidak ingin dirubah</p>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputSkills" class="col-sm-2 control-label">No. Telp</label>

                      <div class="col-sm-10">
                        <input type="number" class="form-control" name="no_telp" id="inputSkills" value="{{ Auth::user()->created_at }}">
                      </div>
                    </div>
                     
                    
                    <hr>
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary">Update</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Modal -->

<!-- /.content -->
@stop
@section('script')

@stop