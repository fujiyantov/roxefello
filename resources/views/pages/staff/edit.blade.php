@extends('layouts.index')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">staff</a></li>
                    <li class="breadcrumb-item active">edit</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Flash Message -->
            @include('partials.message')
        <!--/. End Flash Message -->
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
            <div class="card-header">
                <h4>
                    Edit <span class="text-upppercase text-secondary weight-bold">staff.</span>
                </h4>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <?php $waktu = rand('111', '999'); ?>
            <form action="{{ route('staff:update', [substr(base64_encode(substr(md5($staff->id), 0, 10)), 0, 10).$param.substr(base64_encode($waktu), 0, 4) , slug($staff->name)]) }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name </label>
                                <input type="text" name="name" value="{{ $staff->name }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} float-right">
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            @if( Auth::user()->role == 1 )
                            <div class="form-group">
                                <label>Permisson</label>
                                <select name="permisson" id="permisson" class="form-control">
                                    @foreach($role as $data)
                                        <option value="{{ $data->id }}" @if($data->id == $staff->role) selected @endif>{{ $data->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('permisson'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('permisson') }}</strong>
                                </span>
                                @endif
                            </div>
                            @endif

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email" value="{{ $staff->email }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} float-right">
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <br><br>
                            <div class="form-group">
                                <span>*Kosongkan password jika tidak ada perubahan</span><br>
                                <label>Password</label>
                                <input type="password" name="password" value="{{ old('password') }}" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} float-right">
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Password Confirmation</label>
                                <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }} float-right">
                                @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-left">
                    <button type="submit" class="btn btn-secondary">Update</i></button>
                </div>
            </form>
        </div>
        <!-- /.card -->
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@stop
