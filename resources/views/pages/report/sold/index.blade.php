@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">sold</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <!-- Main content -->
                <div class="invoice p-3 mb-3">

                    <div class="row mb-3 mt-3">
                        <div class="col-12">
                            <h4>
                                <i class="fa fa-cubes"></i> Qty <span class="text-upppercase text-secondary weight-bold">Sold</span>.
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <div class="form-group">
                                <input type="text" class="datepicker_bulan_tahun form-control date-qty" name="tahun" value="{{ old('tahun') }}" placeholder="mm/yyyy">
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <button class="btn btn-secondary btn-block btn-filter"><i class="fa fa-search"></i> Filter</button>
                            </div>
                        </div>
                        <div class="col-12 table-responsive fetch-qty">
                            @include('pages.report.sold.qty')
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.invoice -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@stop
@section('script')
<script>
    $('.btn-filter').click(function(event) {
        /* Act on the event */
        var qty = $('.date-qty').val();

        $.ajax({
            type    :'POST',
            url     :'{{ route('sold:qty') }}',
            data    :{
                _token   : '{!! csrf_token() !!}',
                _date    : qty,
            },
            beforeSend: function(response){

            },
            success: function(response){
                $('.fetch-qty').html(response);
            },
            error: function(response, textStatus, xhr){
                console.log(xhr.textStatus);
            }
        });
    });    
</script>
@stop