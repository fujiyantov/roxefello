<table class="table table-bordered table-striped">
    <tr>
        <th>#</th>
        <th style="min-width: 100px">SKU</th>
        <th>SUM of QTY</th>
        <th>SUM of EXT. COGS</th>
        <th>SUM of EXT. RETAIL PRICE</th>
    </tr>
    <?php 
        $qty    = 0;
        $cogs   = 0;
        $retail = 0;
    ?>
    @foreach($sold as $data)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $data->item->sku }}</td>
            <td>{{ $data->summary_qty($data->sku) }}</td>
            <td>{{ 'Rp '.number_format($data->summary_cogs($data->sku)) }}</td>
            <td>{{ 'Rp '.number_format($data->summary_retail($data->sku) ) }}</td>
            <?php 
                $qty    += $data->summary_qty($data->sku);
                $cogs   += $data->summary_cogs($data->sku);
                $retail += $data->summary_retail($data->sku);
            ?>
        </tr>
    @endforeach
    <tr>
        <td colspan="2" align="right"><strong>Grand Total</strong></td>
        <td><strong>{{ number_format($qty) }}</strong></td>
        <td><strong>{{ 'Rp '.number_format($cogs) }}</strong></td>
        <td><strong>{{ 'Rp '.number_format($retail) }}</strong></td>
    </tr>
</table>