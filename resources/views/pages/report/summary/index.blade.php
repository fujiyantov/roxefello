@extends('layouts.index')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Summary</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <!-- Main content -->
                <div class="invoice p-3 mb-3">

                    <div class="row mb-3 mt-3">
                        <div class="col-12">
                            <h4>
                                <i class="fa fa-cubes"></i> Summary <span class="text-upppercase text-secondary weight-bold">Sold</span>.
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="row mt-4">
                        <!-- Periode -->
                        <div class="col-2">
                            <label for="monthly" class="col-2 control-label">Periode</label>
                        </div>
                        <div class="col-10">
                            <select name="periode" id="periode" class="form-control periode">
                                <option value="1">Daily</option>
                                <option value="2">Monthly</option>
                            </select>
                        </div>
                        <br><br>

                        <!-- GGO -->
                        <div class="col-2 tanggal_label">
                            <label for="monthly" class="col-2 control-label">Date</label>
                        </div>
                        <div class="col-5 tanggal">
                            <input type="text" class="datepicker form-control start_at" name="start" placeholder="Start At: YYYY-MM-DD">
                        </div>
                        <div class="col-5 tanggal">
                            <input type="text" class="datepicker form-control end_at" name="end" placeholder="End At: YYYY-MM-DD">
                        </div>
                        <br><br>


                        <!-- Monthly -->
                        <div class="col-2 tahun_label">
                            <label for="monthly" class="col-2 control-label">Month/Year</label>
                        </div>
                        <div class="col-10 tahun">
                            <input type="text" class="datepicker_bulan_tahun form-control monthly" name="tahun" placeholder="mm/yyyy">
                        </div>

                        <div class="col-12">
                            <hr>
                            <div class="form-group text-right">
                               {{--  <!-- PRINT -->
                                <a href="invoice-print.html" target="_blank" class="btn btn-default float-right"><i class="fa fa-print"></i> Print</a>

                                <!-- PDF -->
                                <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                                    <i class="fa fa-download"></i> Generate PDF
                                </button> --}}

                                <!-- Filter -->
                                <button class="btn btn-success mr-2 btn-filter"><i class="fa fa-search"></i> Filter</button>
                            </div>
                        </div>
                        <div class="col-12 table-responsive fetch-summary">
                            @include('pages.report.summary.summary')
                        </div>
                    </div>

                </div>
                <!-- /.invoice -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@stop
@section('script')
<!-- Page script -->
<script>
    $(function () {
        $('.tahun_label').hide();
        $('.tahun').hide();

        $('.periode').change(function(){
            var periode = $('.periode').val();

            if (periode == 1) {
                $('.tahun_label').hide();
                $('.tahun').hide();

                $('.tanggal_label').show();
                $('.tanggal').show();
            }else{
                $('.tanggal_label').hide();
                $('.tanggal').hide();

                $('.tahun_label').show();
                $('.tahun').show();
            }
        });

        $('.btn-filter').click(function(event) {
            /* Act on the event */
            var start_at  = '';
            var end_at    = '';
            var monthly   = '';

            var periode = $('.periode').val();
            if (periode == 1) {
                start_at    = $('.start_at').val();
                end_at      = $('.end_at').val();

                if (start_at == '' || end_at == '') {alert('Please, select the date!'); return false}

                if (start_at > end_at ) {alert('Start date is greater than the end date!'); return false}

            }else{
                monthly     = $('.monthly').val();

                if (monthly == '') {alert('Please, select the month and year!'); return false}
            }

            $.ajax({
                type    :'POST',
                url     :'{{ route('summary:filter') }}',
                data    :{
                    _token      : '{!! csrf_token() !!}',
                    _periode    : periode,
                    _start_at   : start_at,
                    _end_at     : end_at,
                    _monthly    : monthly,
                },
                beforeSend: function(response){
                    $('.btn-filter').text('Loading...');
                    $('.btn-filter').prop('disabled', true);
                },
                success: function(response){
                    console.log(response);
                    $('.btn-filter').text('Filter');
                    $('.btn-filter').prop('disabled', false);
                    $('.fetch-summary').html(response);
                },
                error: function(response, textStatus, xhr){
                    console.log(xhr.textStatus);
                }
            });
        }); 

        //Date range picker
        $('.datepicker').datepicker({
          format: "yyyy-mm-dd",
        });
    })
</script>
@stop