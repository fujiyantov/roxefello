@if($summary != '')
    <table class="table table-bordered text-13">
        <tr style="background: #f4f4f4!important">
            <th style="min-width: 200px;">Brand</th>
            <th>Item</th>
            <th>Color</th>
            <th>Weight</th>
            <th style="min-width: 100px">SKU</th>
            <th>SUM of QTY</th>
            <th>SUM of EXT. COGS</th>
            <th>SUM of EXT. RETAIL PRICE</th>
        </tr>
        <?php 
            $qty    = 0;
            $cogs   = 0;
            $retail = 0;

            $total_qty      = 0;
            $total_cogs     = 0;
            $total_retail   = 0;
        ?>
        @foreach($summary as $data)
        <!-- tbody -->
            <?php 
                $row_brand = $data->item($data->id); 
                Session::put('brand'.$data->name, $data->name);
                $qty    = 0;
                $cogs   = 0;
                $retail = 0;

            ?>
            @if($data->item($data->id) > 0)
                @foreach($data->detail as $result)
                    @if ($result->summary_qty($result->id) > 0)
                        <tr>
                            <td>{{ $data->name }}</td>
                            <td>{{ $result->name }}</td>
                            <td>{{ $result->color }}</td>
                            <td>{{ $result->weight }}</td>
                            <td><a href="#">{{ $result->sku }}</a></td>
                            <td>{{ number_format($result->summary_qty($result->id)) }}</td>
                            <td>{{ 'Rp '.number_format($result->summary_cogs($result->id)) }}</td>
                            <td>{{ 'Rp '.number_format($result->summary_retail($result->id) ) }}</td>
                        </tr>
                    @endif
                    <?php 
                        $qty    += $result->summary_qty($result->id);
                        $cogs   += $result->summary_cogs($result->id);
                        $retail += $result->summary_retail($result->id);
                    ?>
                @endforeach
            @endif

            
            
            @if($data->item($data->id) > 0)
                <!-- total brand -->
                <tr style="background: #f4f4f4!important">
                    <td colspan="5"><strong>{{ Session::get('brand'.$data->name) }}</strong></td>
                    <td><strong>{{ number_format($qty) }}</strong></td>
                    <td><strong>{{ 'Rp '.number_format($cogs) }}</strong></td>
                    <td><strong>{{ 'Rp '.number_format($retail) }}</strong></td>
                </tr>
                <?php 
                    $total_qty    += $qty;
                    $total_cogs   += $cogs;
                    $total_retail += $retail;
                ?>
            @endif
        @endforeach
            <!-- total brand -->
            <tr style="background: #ddd!important">
                <td colspan="5"><strong>Grand Total</strong></td>
                <td><strong>{{ number_format($total_qty) }}</strong></td>
                <td><strong>{{ 'Rp '.number_format($total_cogs) }}</strong></td>
                <td><strong>{{ 'Rp '.number_format($total_retail) }}</strong></td>
            </tr>
    </table>
@else
    <div class="alert alert-info">
        <h4 class="text-center m-5"><span class="text-xs">no record</span> <span class="text weight-bold">TRANSACTION</span>.</h4>
    </div>
@endif