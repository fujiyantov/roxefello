<?php
use App\User;
use App\SalesDetail;
use App\Stock;
use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
ini_set('max_execution_time', 3000);
ini_set('memory_limit', '-1');

Route::get('/getAdmin', function(){
    $user = new User;
    $user->role = 1;
    $user->name = 'dev';
    $user->email = 'dev@app.com';
    $user->password = Hash::make('password');
    $user->save();
});

Route::get('/sumCogs', function(){
    $all = SalesDetail::all();
    foreach ($all as $data) {
        $detail = SalesDetail::find($data->id);
        $detail->total_cogs = $detail->qty * $detail->cogs;
        $detail->save();
    }
});

Route::get('/cekCogs', function(){
    $all = SalesDetail::all();
    foreach ($all as $data) {
        if($data->cogs > $data->retail){
            echo $data->id.'<br>';
        }
    }
});

Route::get('/updateCogs', function(){
    $all = SalesDetail::all();
    foreach ($all as $data) {
        $stock = Stock::where('item', $data->sku)->orderBy('created_at', 'DESC')->first();
        $detail = SalesDetail::find($data->id);
        $detail->cogs       = $stock->cogs;
        $detail->total_cogs = $detail->qty * $stock->cogs;
        $detail->save();
    }
});

Route::get('/cekstatus', 'StockController@getStatus');
Route::get('/dadakan', 'SalesController@getDadakan')->name('dadakan:get');
Route::get('/urutDadakan', 'SalesController@parentChildDadakan')->name('dadakan:urut:get');
Route::post('/dadakan/excel', 'SalesController@postDadakan')->name('dadakan:post');

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/devel/info', function () {
    phpinfo();
})->name('info');

Auth::routes();

Route::get('/home', 'HomeController@index')->middleware('active')->name('home');
// Route::get('/home/current', 'HomeController@current')->middleware('active')->name('current');

Route::post('/home/summary', 'HomeController@summary')->name('dashboard:summary');

Route::group(['middleware' => 'auth'], function() {
   // Sales
   Route::group(['prefix' => 'sales'], function() {
       Route::get('/', 'SalesController@index')->name('sales:index');
       Route::get('/show/{id}/{slug}', 'SalesController@show')->name('sales:show');
       Route::get('/create', 'SalesController@create')->name('sales:create');
       Route::post('/create/store', 'SalesController@store')->name('sales:store');
       Route::get('/edit/{id}/{slug}', 'SalesController@edit')->name('sales:edit');
       Route::post('/edit/update/{id}/{slug}', 'SalesController@update')->name('sales:update');
       Route::get('/destroy/{id}', 'SalesController@destroy')->name('sales:destroy');
       Route::get('/actived/{id}/{slug}', 'SalesController@actived')->name('sales:actived');

       Route::post('/brand_code', 'SalesController@brand_code')->name('sales:code');
       Route::post('/shopee/excel', 'SalesController@excel')->name('sales:excel');
       Route::post('/tokped/excel', 'SalesController@tokped')->name('sales:tokped');
       Route::post('/print', 'SalesController@print')->name('sales:print');
       Route::post('/filter', 'SalesController@filter')->name('sales:filter');
       Route::post('/check', 'SalesController@check')->name('sales:check');
       Route::post('/accepted', 'SalesController@accepted')->name('sales:accepted');
   });

    // Merchandise
    Route::group(['prefix' => 'item'], function() {
        Route::get('/', 'ItemController@index')->name('item:index');
        Route::get('/show/{id}/{slug}', 'ItemController@show')->name('item:show');
        Route::get('/create', 'ItemController@create')->name('item:create');
        Route::post('/create/store', 'ItemController@store')->name('item:store');
        Route::get('/edit/{id}/{slug}', 'ItemController@edit')->name('item:edit');
        Route::post('/edit/update{id}/{slug}', 'ItemController@update')->name('item:update');
        Route::get('/destroy/{id}/{slug}', 'ItemController@destroy')->name('item:destroy');
        Route::get('/actived/{id}/{slug}', 'ItemController@actived')->name('item:actived');

        Route::post('/brand_code', 'ItemController@brand_code')->name('item:code');
        Route::post('/filter', 'ItemController@filter')->name('item:filter');
    });

    // Stock
    Route::group(['prefix' => 'stock'], function() {
        Route::get('/', 'StockController@index')->name('stock:index');
        Route::get('/show/{id}/{slug}', 'StockController@show')->name('stock:show');
        Route::get('/create', 'StockController@create')->name('stock:create');
        Route::post('/create/store', 'StockController@store')->name('stock:store');
        Route::get('/edit/{id}/{slug}', 'StockController@edit')->name('stock:edit');
        Route::post('/edit/update{id}/{slug}', 'StockController@update')->name('stock:update');
        Route::get('/destroy/{id}', 'StockController@destroy')->name('stock:destroy');
        Route::get('/actived/{id}/{slug}', 'StockController@actived')->name('stock:actived');

        Route::post('/sold', 'StockController@sold')->name('stock:sold');
    });

    // Sold
    Route::group(['prefix' => 'sold'], function() {
        Route::get('/', 'SoldController@index')->name('sold:index');
        Route::post('/qty', 'SoldController@qty')->name('sold:qty');
    });

    // Summary
    Route::group(['prefix' => 'summary'], function() {
        Route::get('/', 'SummaryController@index')->name('summary:index');
        Route::post('/filter', 'SummaryController@filter')->name('summary:filter');
    });

    // Brand
    Route::group(['prefix' => 'brand'], function() {
        Route::get('/', 'BrandController@index')->name('brand:index');
        Route::get('/show', 'BrandController@show')->name('brand:show');
        Route::get('/create', 'BrandController@create')->name('brand:create');
        Route::post('/create/store', 'BrandController@store')->name('brand:store');
        Route::get('/edit/{id}/{slug}', 'BrandController@edit')->name('brand:edit');
        Route::post('/edit/update{id}/{slug}', 'BrandController@update')->name('brand:update');
        Route::get('/destroy/{id}/{slug}', 'BrandController@destroy')->name('brand:destroy');
        Route::get('/actived/{id}/{slug}', 'BrandController@actived')->name('brand:actived');
    });

    // Category
    Route::group(['prefix' => 'category'], function() {
        Route::get('/', 'CategoryController@index')->name('category:index');
        Route::get('/show', 'CategoryController@show')->name('category:show');
        Route::get('/create', 'CategoryController@create')->name('category:create');
        Route::post('/create/store', 'CategoryController@store')->name('category:store');
        Route::get('/edit/{id}/{slug}', 'CategoryController@edit')->name('category:edit');
        Route::post('/edit/update{id}/{slug}', 'CategoryController@update')->name('category:update');
        Route::get('/destroy/{id}/{slug}', 'CategoryController@destroy')->name('category:destroy');
        Route::get('/actived/{id}/{slug}', 'CategoryController@actived')->name('category:actived');
    });

    // Staff
    Route::group(['prefix' => 'staff'], function() {
        Route::get('/', 'StaffController@index')->name('staff:index');
        Route::get('/show/{id}/{slug}', 'StaffController@show')->name('staff:show');
        Route::get('/create', 'StaffController@create')->name('staff:create');
        Route::post('/create/store', 'StaffController@store')->name('staff:store');
        Route::get('/edit/{id}/{slug}', 'StaffController@edit')->name('staff:edit');
        Route::post('/edit/update/{id}/{slug}', 'StaffController@update')->name('staff:update');
        Route::get('/destroy/{id}/{slug}', 'StaffController@destroy')->name('staff:destroy');
        Route::get('/actived/{id}/{slug}', 'StaffController@actived')->name('staff:actived');

        Route::get('/backup', 'StaffController@backup')->name('staff:backup');
        Route::post('/restore', 'StaffController@restore')->name('staff:restore');
    });

});

