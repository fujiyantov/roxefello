<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Brand;
use App\Category;
use App\Stock;
use App\Sales;
use App\SalesDetail;
use DB, Session;

class Item extends Model
{
    protected $table = 'merchandises';
    public $dates = ['create_at', 'updated_at'];

    public function createdBy(){
    	return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedBy(){
    	return $this->belongsTo('App\User', 'updated_by');
    }

    public function brand(){
    	return $this->belongsTo('App\Brand', 'brand_id');
    }

    public function category(){
    	return $this->belongsTo('App\Category', 'category_id');
    }

    public function stock($item){
    	return $this->hasMany('App\Stock', 'item')->whereItem($item)->orderBy('created_at', 'ASC')->sum('qty');
    }

    public function remaining($item){
        return $this->hasMany('App\Stock', 'item')->whereItem($item)->orderBy('created_at', 'ASC')->sum('remaining');
    }

    public function remainingShare($id)
    {
        $get = Item::where('id', $id)->first();
        $weight = $get->weight;
        
        $qty = Self::stock($id);
        $remaining = Self::remaining($id);

        $total = $qty * $weight;
        $grand = $remaining - $total;
        //return $grand;
        if ($grand == 0) {
            $remainingShare = 0;    
        }else{
            $remainingShare = $grand;    
        }
        
        return $remainingShare;
    } 

    static function qty($id)
    {
        return SalesDetail::where('sku', $id)->sum('qty');
    } 

    static function cogs($id)
    {
        return SalesDetail::where('sku', $id)->sum('cogs');
    } 

    static function retail($id)
    {
        return SalesDetail::where('sku', $id)->sum('retail');
    }

    static function summary_qty($sku)
    {
        if(Session::get('periode') == 1){
            return SalesDetail::where('created_at', '>=', Session::get('start_at').' 00:00:00')->where('created_at', '<=', Session::get('end_at').' 23:59:59')->where('sku', $sku)->sum('qty');
        }else{
            return SalesDetail::whereMonth('created_at', Session::get('month'))->whereYear('created_at', Session::get('year'))->where('sku', $sku)->sum('qty');
        }
    }

    static function summary_cogs($sku)
    {
        if(Session::get('periode') == 1){
            return SalesDetail::where('created_at', '>=', Session::get('start_at').' 00:00:00')->where('created_at', '<=', Session::get('end_at').' 23:59:59')->where('sku', $sku)->sum('total_cogs');
        }else{
            return SalesDetail::whereMonth('created_at', Session::get('month'))->whereYear('created_at', Session::get('year'))->where('sku', $sku)->sum('total_cogs');
        }
    }

    static function summary_retail($sku)
    {
        if(Session::get('periode') == 1){
            return SalesDetail::where('created_at', '>=', Session::get('start_at').' 00:00:00')->where('created_at', '<=', Session::get('end_at').' 23:59:59')->where('sku', $sku)->sum('total');
        }else{
            return SalesDetail::whereMonth('created_at', Session::get('month'))->whereYear('created_at', Session::get('year'))->where('sku', $sku)->sum('total');
        }
    }

    static function filter($data)
    {
        $_type      = '';
        $_name      = '';
        $_sku       = '';
        $_size      = '';
        $_brand     = '';
        $_category  = '';
        $_max       = '';
        $_min       = '';
        $_range_at  = '';

        if ($data['_type'] != '') {
            $_type = " and type = '".$data['_type']."'";
        }

        if ($data['_name'] != '') {
            $_name = " and name like '%".$data['_name']."%'";
            //dd($_name);
        }

        if ($data['_sku'] != '') {
            $_sku = " and sku like '%".$data['_sku']."%'";;
        }

        if ($data['_size'] != '') {
            $_size = " and size = '".$data['_size']."'";
        }

        if ($data['_brand'] != '') {
           $_brand = " and brand_id = '".$data['_brand']."'";
        }

        if ($data['_category'] != '') {
            $_category = " and category_id = '".$data['_category']."'";
        }

        if ($data['_max'] != '' && $data['_min'] == '') {
            $_max = " and b.qty <= '".$data['_max']."'";
        }

        if ($data['_min'] != '' && $data['_max'] == '') {
            $_min = " and b.qty >= '".$data['_min']."'";
        }


        if ($data['_min'] != '' && $data['_max'] != '') {
            $_range_at = " and b.qty >= '".$data['_min']."' and b.qty <= '".$data['_max']."'";
        }

        $data = DB::SELECT("SELECT a.id FROM merchandises a LEFT JOIN stocks b ON a.id = b.item
            WHERE a.is_published = '1'
            ".$_type."
            ".$_name."
            ".$_sku."
            ".$_size."
            ".$_brand."
            ".$_category."
            ".$_max."
            ".$_min."
            ".$_range_at."
            ");
        return $data;
    }   
}
