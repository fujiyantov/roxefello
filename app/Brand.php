<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    public function createdBy(){
    	return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedBy(){
    	return $this->belongsTo('App\User', 'updated_by');
    }

    public function item()
    {
       return $this->hasMany('App\Item', 'brand_id')->count();
    }

    public function sku($id)
    {
       return $this->hasMany('App\Item', 'brand_id')->select('id')->first();
    }

    public function itemId($id)
    {
       $var = $this->hasMany('App\Item', 'brand_id')->select('id')->first();
       return $var->id;
    }

    public function detail()
    {
      return $this->hasMany('App\Item', 'brand_id');
    }
}
