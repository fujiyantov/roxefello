<?php

namespace App\Imports;

use App\Sales;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithMappedCells;

class SalesImport implements WithMappedCells, ToModel 
{
    public $count = 0;
    
    public function mapping(): array
    {
        return [
            'order_number'  => 'A3',
        ];
    }
    
    public function model(array $row)
    {
        return new Sales([
            'order_number' => $row['order_number'],
        ]);
    }
}
