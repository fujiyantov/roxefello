<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sales;
use Session;

class SalesDetail extends Model
{
    protected $table = 'sales_detail';
    public $dates = ['created_at', 'updated_at'];

    public function item(){
    	return $this->belongsTo('App\Item', 'sku');
    }

    public function createdBy(){
    	return $this->belongsTo('App\User', 'created_by');
    }

    public function sales()
    {
    	return $this->belongsTo('App\Sales', 'sales');
    }

    static function qty($sku)
    {
        return SalesDetail::where('sku', $sku)->sum('qty');
    }

    static function cogs($sku)
    {
        return SalesDetail::where('sku', $sku)->sum('cogs');
    }

    static function retail($sku)
    {
        return SalesDetail::where('sku', $sku)->sum('retail');
    }

    static function sold($sku, $month, $day)
    {
        // return SalesDetail::where('sku', $sku)->whereMonth('created_at', $month)->whereDay('created_at', $day)->sum('qty');
        

        return Sales::join('sales_detail', 'sales_detail.sales', '=', 'sales.id')->where('sales_detail.sku', $sku)->whereMonth('sales.order_date', $month)->whereDay('sales.order_date', $day)->sum('sales_detail.qty');
    }

    static function summary_qty($sku)
    {
        
        return SalesDetail::whereMonth('created_at', Session::get('month'))->whereYear('created_at', Session::get('year'))->where('sku', $sku)->sum('qty');
        
    }

    static function summary_cogs($sku)
    {
        
        return SalesDetail::whereMonth('created_at', Session::get('month'))->whereYear('created_at', Session::get('year'))->where('sku', $sku)->sum('total_cogs');
        
    }

    static function summary_retail($sku)
    {
        
        return SalesDetail::whereMonth('created_at', Session::get('month'))->whereYear('created_at', Session::get('year'))->where('sku', $sku)->sum('total');
        
    }
}
