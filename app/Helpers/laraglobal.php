<?php
use Carbon\Carbon;
use App\Sales;

function setActive($path, $class = 'active')
{
    if (is_array($path)) {
        foreach ($path as $item) {
            if (Request::is($item)) {
                return $class;
            }
        }
    } else {
        return (Request::is($path)) ? $class : '';
    }
}

function setOpen($path, $class = 'menu-open')
{
    if (is_array($path)) {
        foreach ($path as $item) {
            if (Request::is($item)) {
                return $class;
            }
        }
    } else {
        return (Request::is($path)) ? $class : '';
    }
}

function slug($title)
{
    return strtolower(preg_replace('/[^\w]+/', '-', $title));
}

function trx_code()
{   
    $cek = Sales::selectRaw('count(id) as total')->where('order_type', 2)->get();
    // dd($cek);
    //return $cek[0]->total;
    if($cek[0]->total == 0)
    {
        $bulan_sekarang     = date('m');
        $tahun_sekarang     = date('y');
        $tanggal_sekarang   = date('d');
        return 'TL'.$tahun_sekarang.$bulan_sekarang.$tanggal_sekarang.str_pad(1, 4, 0, STR_PAD_LEFT);
    }
    else
    {   
        $trx                = Sales::take(1)->orderby('id', 'desc')->where('order_type', 2)->first();
        $code               = $trx->order_number;
        $bulan_sekarang     = date('m');
        $tahun_sekarang     = date('y');
        $tanggal_sekarang   = date('d');
        $tahun              = substr($code, 2, 2);

        $bulan              = substr($code, 4, 2);
        $tanggal            = substr($code, 6, 2);
        $urutan             = intval(substr($code, -4));
        if($bulan_sekarang != $bulan || $tahun_sekarang != $tahun || $tanggal_sekarang != $tanggal)
        {
            $next       = 1;
        }
        else
        {
            $next       = $urutan+1;
        }
        //dd('TL'.$tahun_sekarang.$bulan_sekarang.$tanggal_sekarang.str_pad($next, 4, 0, STR_PAD_LEFT));
        return 'TL'.$tahun_sekarang.$bulan_sekarang.$tanggal_sekarang.str_pad($next, 4, 0, STR_PAD_LEFT);
    }

}