<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stocks';
    public $dates = ['created_at', 'updated_at', 'purchase_date'];

    public function createdBy(){
    	return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedBy(){
    	return $this->belongsTo('App\User', 'updated_by');
    }

    public function stockShare($id)
    {
        $get = Item::where('id', $id)->first();
        $weight = $get->weight;
        
        $qty = Self::stock($id);
        $remaining = Self::remaining($id);

        $total = $qty * $weight;
        $grand = $remaining - $total;
        //return $grand;
        if ($grand == 0) {
            $remainingShare = 0;    
        }else{
            $remainingShare = $grand;    
        }
        
        return $remainingShare;
    } 
}
