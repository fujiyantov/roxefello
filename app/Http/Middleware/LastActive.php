<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use \Cache;

class LastActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* DB */
        if (!Auth::check()) {
            return $next($request);
        }
        
        // $redis  = Redis::connection();
        // $key    = 'last_active_' . Auth::id();

        // if ($redis->get($key) == '') {

            $time = date('Y-m-d H:i:s');
            $user = Auth::user();
            $user->last_active = $time;
            $user->save();

            return $next($request);

        // }else{

        //     $value  = date('Y-m-d h:i:s');
        //     $redis->set($key, $value);
        //     return $next($request);    
        // }
    }
}
