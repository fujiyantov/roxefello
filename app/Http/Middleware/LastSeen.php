<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use \Cache;

class LastSeen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* Redis */
        // if (!Auth::check()) {
        //     return $next($request);
        // }

        // $redis  = Redis::connection();
        // $key    = 'last_seen_' . Auth::id();
        // $value  = date('Y-m-d h:i:s');
      
        // $redis->set($key, $value);
        // return $next($request);

        /* DB */
        if (!Auth::check()) {
            return $next($request);
        }
        
        $time = date('Y-m-d H:i:s');
        $user = Auth::user();
        $user->last_seen = $time;
        $user->save();

        return $next($request);
        
        /* Cache */
        // if(Auth::check()) {
        //     $expiresAt = Carbon::now()->addMinutes(1);
        //     Cache::put('user-is-online-' . Auth::user()->id, true, $expiresAt);
        // }
        // return $next($request);
    }
}
