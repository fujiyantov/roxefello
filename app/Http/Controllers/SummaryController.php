<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Brand;
use App\Category;
use App\Item;
use App\Stock;
use App\Sales;
use App\SalesDetail;

use DB, Validator, Auth, Session;

class SummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['summary']   = SalesDetail::whereNotNull('sku')
        //             ->whereMonth('created_at', Carbon::now()->month)
        //             ->whereYear('created_at', Carbon::now()->year)
        //             ->distinct('sku')
        //             ->select('sku')
        //             ->get();

        // $sales_detail = SalesDetail::whereDay('created_at', date('d'))->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->select('sku')->get();

        // Session::put('periode', 2);
        // Session::put('month', date('m'));
        // Session::put('year', date('Y'));

        $sales_detail = SalesDetail::where('created_at', '>=', date('Y-m-d').' 00:00:00')->where('created_at', '<=', date('Y-m-d').' 23:59:59')->select('sku')->get();
        // return $sales_detail;
        foreach ($sales_detail as $data) {
            $sku[]=$data->sku;
        }

        Session::put('periode', 1);
        Session::put('start_at', date('Y-m-d'));
        Session::put('end_at', date('Y-m-d'));

        $data['summary'] = '';

        if (isset($sku)) {
            $item = Item::whereIn('id', $sku)->select('brand_id')->get();
            foreach ($item as $data) {
                $brand[]=$data->brand_id;
            }
            
            $data['summary']   = Brand::whereIn('id', $brand)->where('is_published', 1)->get();
            // return $data['summary'];
        }

        $date = date('Y-m-d');

        return view('pages.report.summary.index', $data, compact('date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function filter(Request $request){

        //Daily
        if ($request->input('_periode') == 1) {

            $sales_detail = SalesDetail::where('created_at', '>=', $request->input('_start_at').' 00:00:00')->where('created_at', '<=', $request->input('_end_at').' 23:59:59')->select('sku')->get();

            foreach ($sales_detail as $data) {
                $sku[]=$data->sku;
            }

            Session::put('periode', 1);
            Session::put('start_at', $request->input('_start_at'));
            Session::put('end_at', $request->input('_end_at'));
        }else{
            //Monthly
            $time  = explode('/', $request->get('_monthly'));
            $data['month'] = $time[0];
            $data['year']  = $time[1];

            Session::put('periode', 2);
            Session::put('month', $data['month']);
            Session::put('year', $data['year']);
            // return Session::get('year');
            $sales_detail = SalesDetail::whereMonth('created_at', $data['month'])->whereYear('created_at', $data['year'])->select('sku')->get();
            
            foreach ($sales_detail as $data) {
                $sku[]=$data->sku;
            }
        }

        $data['summary'] = '';

        if (isset($sku)) {
            $item = Item::whereIn('id', $sku)->select('brand_id')->get();
            foreach ($item as $data) {
                $brand[]=$data->brand_id;
            }
            
            $data['summary']   = Brand::whereIn('id', $brand)->where('is_published', 1)->get();    
        }

        return view('pages.report.summary.summary', $data);
    }
}
