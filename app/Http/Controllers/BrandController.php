<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Category;

use DB, Validator, Auth;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['brand'] = Brand::orderBy('created_at', 'DESC')->get();
        return view('pages.brand.index', $data)->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:3|max:255|unique:brands',
            'code'  => 'required|min:2|unique:brands'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Please fix the error(s) below');
        }

        $brand                 = New Brand;
        $brand->name           = $request->input('name');
        $brand->code           = strtoupper($request->input('code'));
        $brand->created_by     = Auth::id();
        $brand->is_published   = 1;
        $brand->save();

        if ($brand) {
            return redirect(route('brand:index'))->with('success', 'Brand Saved Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please try again');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['brand']  = Brand::findOrFail($id);
        return view('pages.brand.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|min:3|max:255',
            'code'  => 'required|min:2'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Please fix the error(s) below');
        }

        $brand                 = Brand::findOrFail($id);
        $brand->name           = $request->input('name');
        $brand->code           = $request->input('code');
        $brand->updated_by     = Auth::id();
        $brand->is_published   = 1;
        $brand->save();

        if ($brand) {
            return redirect(route('brand:index'))->with('success', 'Brand Updated Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please try again');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand                 = Brand::findOrFail($id);
        $brand->updated_by     = Auth::id();
        $brand->is_published   = 0;
        $brand->save();

        if ($brand) {
            return redirect(route('brand:index'))->with('success', 'Brand Unpublish Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please try again');
        }
    }

    public function actived($id)
    {
        $brand                 = Brand::findOrFail($id);
        $brand->updated_by     = Auth::id();
        $brand->is_published   = 1;
        $brand->save();

        if ($brand) {
            return redirect(route('brand:index'))->with('success', 'Brand Publish Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please try again');
        }
    }
}
