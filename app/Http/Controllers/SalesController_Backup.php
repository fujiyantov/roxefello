<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\SalesImport;
use App\Brand;
use App\Category;
use App\Item;
use App\Stock;
use App\Sales;
use App\SalesDetail;

use DB, Validator, Auth, Session;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sales'] = Sales::where('is_published', 1)->orderBy('created_at', 'DESC')->get();
        return view('pages.sales.index', $data)->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $data['trx_code']   = trx_code(); 
        $share_only         = Item::where('is_published', 1)->where('type', 1)->where('size', 2)->get();
        $count = count($share_only);
        //dd($count);
        if ($count > 0) {
            foreach ($share_only as $value) {
                $res[] = $value->id;
            }    
        }else{
            $res = array(0);
        }
        

        //dd($res);

        $data['item']       = Item::where('is_published', 1)->whereNotIn('id', $res)->orderBy('name', 'ASC')->get();
        //$data['item']       = Item::whereNull('sub')->get();
        // dd($data['item']);
        return view('pages.sales.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->input('customer_address'));
        $rules = [
            'order_number'  => 'required|unique:sales',
            'order_date'  => 'required',
            'shipment'  => 'required',
            'customer_name'  => 'required',
            'customer_address'  => 'required',
            'customer_contact'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Please fix the error(s) below');
        }
        
        /*Order*/
        $order_type         = $request->input('order_type');
        $order_number       = $request->input('order_number');
        $order_date         = $request->input('order_date');
        $shipment           = $request->input('shipment');
        $shipment_type      = $request->input('shipment_type');

        /*Customer*/
        $customer_name      = $request->input('customer_name');
        $customer_contact   = $request->input('customer_contact');
        $customer_address   = $request->input('customer_address');

        //Sales
        $sales  = new Sales;
        $sales->order_type          = 2;
        $sales->order_number        = $order_number;
        $sales->order_date          = $order_date;
        $sales->customer_name       = $customer_name;
        $sales->customer_contact    = $customer_contact;
        $sales->customer_address    = $customer_address;
        $sales->shipment            = $shipment;
        $sales->shipment_type       = $shipment_type;
        $sales->is_published        = 1;
        $sales->created_by          = Auth::id();
        $sales->save();

        /*item*/
        $i = 0;
        //$sales_item      = $request->input('sales_item'); //name item
        $item_id         = $request->input('item_id'); //item id -> looping
        foreach ($item_id as $data) {
            if ($request->input('split')[$i] != NULL) {
                /*Pembelian Share*/
                //dd('share');

                $x = 0;
                $share_weight   = $request->input('share_weight_'.$data);
                $share_qty      = $request->input('share_qty_'.$data);
                //dd($share_qty);
                foreach ($share_weight as $row) {
                    /*looping share*/

                    //check item
                    $item   = Item::where('weight', $row)->first();

                    if (!isset($item)) {
                        //create new item
                        $getItem    = Item::findOrFail($data); //get data item 

                        $brand      = Brand::findOrFail($getItem->brand_id);
                        $category   = Category::findOrFail($getItem->category_id);

                        $unit       = ($getItem->unit == 1)? 'gr' : 'ml';
                        $name_item  = $brand->name.' '.$getItem->item.' '.$getItem->color.' '.$category->name.' Share '.$row.' '.$unit;
                        $slug       = slug($name_item);

                        $item                   = New Item; 
                        $item->brand_id         = $getItem->brand_id;
                        $item->category_id      = $getItem->category_id;
                        $item->name             = $name_item;
                        $item->slug             = $slug;
                        $item->sub              = $getItem->id;
                        $item->sku              = $getItem->sku.'-'.$row; //create sub SKU
                        $item->size             = 2;
                        $item->weight           = $row; //share_weight
                        $item->item             = $getItem->item;
                        $item->color            = $getItem->color;
                        $item->unit             = $getItem->unit;
                        $item->description      = $getItem->description;
                        $item->created_by       = Auth::id();
                        $item->is_published     = 1;
                        $item->save();
                    }

                    //get data stock where qty > 0 and weight(sisa dari share masih ada) > 0
                    $getStock = Stock::where('item', $data)->where('qty', '>', 0)->where('remaining', '>', 0)->orderBy('created_at', 'ASC')->first();

                    // $re_cogs = ($row * $getStock->cogs) / $getItem->weight;
                    // dd($re_cogs);

                    $stock  = New Stock;
                    $stock->item          = $item->id;
                    $stock->purchase_date = date('Y-m-d');
                    // $stock->qty           = $request->input('share_qty_'.$data)[$x]; //share_qty
                    // $stock->remaining     = $row * $request->input('share_qty_'.$data)[$x];
                    $stock->qty           = 0;
                    $stock->remaining     = 0;
                    $stock->cogs          = ($row * $getStock->cogs) / $item->weight;
                    $stock->retail        = ($row * $getStock->retail) / $item->weight;
                    $stock->created_by    = Auth::id();
                    $stock->save();

                    $detail = new SalesDetail;
                    $detail->sales              = $sales->id;
                    $detail->sku                = $item->id;
                    $detail->merchandise        = $item->name;
                    $detail->qty                = $request->input('share_qty_'.$data)[$x]; //share_qty;
                    $detail->cogs               = ($row * $getStock->cogs) / $item->weight;
                    $detail->retail             = ($row * $getStock->retail) / $item->weight;
                    $detail->total              = $request->input('share_qty_'.$data)[$x] * ($row * $getStock->retail) / $item->weight;
                    $detail->created_by         = Auth::id();
                    $detail->save();

                    $tampung[] = $row * $request->input('share_qty_'.$data)[$x++];
                    //dd($tampung);
                }

                $total_weight   = array_sum($tampung); //get total weight
                $getItem        = Item::findOrFail($data); //get data item

                //Define Weight/@item master
                $max = $getItem->weight; //ex:100ml -> pembagi
                $y   = $total_weight/$max;

                /*test looping*/
                $k = 0;
                do {

                    $getStock = Stock::where('item', $data)->where('qty', '>', 0)->where('remaining', '>', 0)->orderBy('created_at', 'ASC')->first();

                    if ($y > $getStock->qty || $total_weight > $getStock->remaining) {
                        
                        $y                   = $y - $getStock->qty ; 
                        $total_weight        = $total_weight - $getStock->remaining;

                        $getStock->qty       = 0;
                        $getStock->remaining = 0;
                        $getStock->save();

                    }else{

                        $decrement_stock     = $getStock->qty - $y; 
                        $decrement           = $getStock->remaining - $total_weight;
                        
                        $getStock->qty       = $decrement_stock;
                        $getStock->remaining = $decrement;
                        $getStock->save();

                        $k++;
                    }
                    //dd($k.'-'.$y.'-'.$total_weight);
                } while ($k < 1);
                
                //increment $i for looping
                $i++;

            }elseif($request->input('package')[$i] != NULL){
                /*Pembelian Package*/
                //dd('package');

                $getItem    = Item::findOrFail($data); //get data item 

                $brand      = Brand::findOrFail($getItem->brand_id);
                $category   = Category::findOrFail($getItem->category_id);

                $unit       = ($getItem->unit == 1)? 'gr' : 'ml';
                $name_item  = $brand->name.' '.$getItem->item.' '.$getItem->color.' '.$category->name.' Package '.$getItem->weight.' '.$unit;
                $slug       = slug($name_item);

                $item                   = New Item; 
                $item->brand_id         = $getItem->brand_id;
                $item->category_id      = $getItem->category_id;
                $item->name             = $name_item;
                $item->slug             = $slug;
                $item->sub              = $getItem->id;
                $item->sku              = $getItem->sku.'-'.$getItem->weight.'P'; //create sub SKU
                $item->size             = 2;
                $item->weight           = $getItem->weight;
                $item->item             = $getItem->item;
                $item->color            = $getItem->color;
                $item->unit             = $getItem->unit;
                $item->description      = $getItem->description;
                $item->created_by       = Auth::id();
                $item->is_published     = 1;
                $item->save();

                //get data stock where qty > 0 and weight(sisa dari share masih ada) > 0
                $getStock = Stock::where('item', $data)->orderBy('created_at', 'ASC')->first(); 

                $stock  = New Stock;
                $stock->item          = $item->id;
                $stock->purchase_date = date('Y-m-d');
                // $stock->qty           = 1; // Seharusnya 1 :D
                // $stock->remaining     = $getStock->remaining;
                $stock->qty           = 0;
                $stock->remaining     = 0;
                $stock->cogs          = ($getStock->remaining * $getStock->cogs) / $getItem->weight; 
                $stock->retail        = $request->input('sales_retail')[$i];
                $stock->created_by    = Auth::id();
                $stock->save();

                $detail = new SalesDetail;
                $detail->sales              = $sales->id;
                $detail->sku                = $item->id;
                $detail->merchandise        = $item->name;
                $detail->qty                = 1; // Seharusnya 1 :D
                $detail->cogs               = ($getStock->remaining * $getStock->cogs) / $getItem->weight;
                $detail->retail             = $request->input('sales_retail')[$i];
                $detail->total              = $request->input('sales_retail')[$i]; 
                $detail->created_by         = Auth::id();
                $detail->save();

                //Sisa Stock
                $getStock->qty       = 0; 
                $getStock->remaining = 0;
                $getStock->save();

                //increment $i for looping
                $i++;

            }else{
                /*Pembelian Normal*/ //share && package == NULL
                //dd('normal');

                $getItem    = Item::findOrFail($data); //get data item
                $getStock   = Stock::where('item', $data)->where('qty', '>', 0)->where('remaining', '>', 0)->orderBy('created_at', 'ASC')->first();

                $detail = new SalesDetail;
                $detail->sales              = $sales->id;
                $detail->sku                = $getItem->id;
                $detail->merchandise        = $getItem->name;
                $detail->qty                = $request->input('sales_qty')[$i]; //sales_qty
                $detail->cogs               = $getStock->cogs;
                $detail->retail             = $getStock->retail;
                $detail->total              = $request->input('sales_qty')[$i] * $getStock->retail;
                $detail->created_by         = Auth::id();
                $detail->save();

                $total_weight   = $getItem->weight * $request->input('sales_qty')[$i];
                $max = $getItem->weight; 
                $y   = $total_weight/$max;
                //dd($y);

                //$y              = $getStock->remaining - $total_weight;
                //$decrement      = $getStock->qty - $request->input('sales_qty')[$i];

                /*test looping*/
                $k = 0;
                do {

                    $getStock = Stock::where('item', $data)->where('qty', '>', 0)->where('remaining', '>', 0)->orderBy('created_at', 'ASC')->first();
                    //dd($getStock);

                    if ($y > $getStock->qty || $total_weight > $getStock->remaining) {
                        //dd('TRUE');
                        
                        $y                   = $y - $getStock->qty ; 
                        $total_weight        = $total_weight - $getStock->remaining;

                        $getStock->qty       = 0;
                        $getStock->remaining = 0;
                        $getStock->save();

                    }else{
                        //dd('FALSE');

                        $decrement_stock     = $getStock->qty - $y; 
                        $decrement           = $getStock->remaining - $total_weight;
                        
                        $getStock->qty       = $decrement_stock;
                        $getStock->remaining = $decrement;
                        $getStock->save();

                        $k++;
                    }
                    //dd($k.'-'.$y.'-'.$total_weight);
                } while ($k < 1);

                /*
                    $total_weight   = $getItem->weight;
                    $y              = $getStock->remaining - $total_weight;
                    $decrement      = $getStock->qty - $request->input('sales_qty')[$i];


                    $getStock->qty       = $decrement;
                    $getStock->remaining = $y;
                    $getStock->save();
                */

                //increment $i for looping
                $i++;
            }
        }
        //dd('Development');
        if ($getStock) {
            return redirect(route('sales:index'))->with('success', 'Sales Saved Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please contact developer');
        }

        // echo 'end looping';
        // exit;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['sales'] = Sales::where('is_published', 1)->where('id', $id)->first();
        $data['item']  = SalesDetail::where('sales', $data['sales']->id)->get();
        //dd($data['sales']->id);
        return view('pages.sales.show', $data)->with('i');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function excel(Request $request)
    {
        // dd($request->file('excel'));

        $excelFile  = $request->file('excel');
        $number     = 0;
        // Excel::load($excelFile, function($reader) {
        //     $number     = 0;
        //     foreach ($reader->toArray() as $row) {
                
        //         /* Cek order_number*/

        //         if (isset($row['no._pesanan'])) {
        //             $check = Sales::where('order_number', $row['no._pesanan'])->first();
        //             $count = $check->id;
        //             if ($count > 0) {
        //                 $number = 1;
                        
        //                 return redirect()->back()->with('error', 'Please fix the error(s) below');
        //             }
        //         }
        //     }
        // });
        //dd('stop');

        Excel::load($excelFile, function($reader) {
            foreach ($reader->toArray() as $row) {
                /*debug*/
                // $bb[] = $row;

                //Live
                if ($row['no._pesanan'] == NULL) {
                    //search item from index nama_produk and nama_variasi
                    $item_name = $row['nama_produk'].' '.$row['nama_variasi'];
                    $slug_item = slug($item_name);
                    //get item
                    $get_item  = Item::where('slug', $slug_item)->first();

                    $detail = new SalesDetail;
                    $detail->sales       = Session::get('sales_id');
                    $detail->sku         = $get_item->id;
                    $detail->merchandise = $row['nama_produk'];
                    $detail->qty         = round($row['jumlah']);
                    $detail->cogs        = preg_replace("/[^0-9]/", "", $row['harga_sebelum_diskon'] );
                    $detail->retail      = preg_replace("/[^0-9]/", "", $row['harga_setelah_diskon'] );
                    $detail->total       = preg_replace("/[^0-9]/", "", $row['total_harga_produk'] );
                    $detail->created_by  = Auth::id();
                    $detail->save();
                }else{

                    $create = new Sales;
                    $create->order_type         = 1;
                    $create->order_number       = $row['no._pesanan'];
                    $create->order_date         = $row['waktu_pembayaran_dilakukan'];
                    $create->customer_name      = $row['nama_penerima'];
                    $create->customer_contact   = $row['no._telepon'];
                    $create->customer_address   = $row['alamat_pengiriman'];
                    $create->shipment           = $row['opsi_pengiriman'];
                    $create->is_published       = 1;
                    $create->created_by         = Auth::id();
                    $create->save();

                    Session::put('sales_id', $create->id);
                    //search item from index nama_produk and nama_variasi
                    $item_name = $row['nama_produk'].' '.$row['nama_variasi'];
                    $slug_item = slug($item_name);
                    //get item
                    $get_item  = Item::where('slug', $slug_item)->first();

                    $detail = new SalesDetail;
                    $detail->sales       = $create->id;
                    $detail->sku         = $get_item->id;
                    $detail->merchandise = $row['nama_produk'];
                    $detail->qty         = round($row['jumlah']);
                    $detail->cogs        = preg_replace("/[^0-9]/", "", $row['harga_sebelum_diskon'] );
                    $detail->retail      = preg_replace("/[^0-9]/", "", $row['harga_setelah_diskon'] );
                    $detail->total       = preg_replace("/[^0-9]/", "", $row['total_harga_produk'] );
                    $detail->created_by  = Auth::id();
                    $detail->save();
                }

                $total_weight   = $get_item->weight * $detail->qty;
                $max = $get_item->weight; 
                $y   = $total_weight/$max;

                $k = 0;
                do {

                    $getStock = Stock::where('item', $get_item->id)->where('qty', '>', 0)->where('remaining', '>', 0)->orderBy('created_at', 'ASC')->first();

                    if ($y > $getStock->qty || $total_weight > $getStock->remaining) {
                        
                        $y                   = $y - $getStock->qty ; 
                        $total_weight        = $total_weight - $getStock->remaining;

                        $getStock->qty       = 0;
                        $getStock->remaining = 0;
                        $getStock->save();

                    }else{

                        $decrement_stock     = $getStock->qty - $y; 
                        $decrement           = $getStock->remaining - $total_weight;
                        
                        $getStock->qty       = $decrement_stock;
                        $getStock->remaining = $decrement;
                        $getStock->save();

                        $k++;
                    }
                    
                } while ($k < 1);
            }
            /* debug */
            // dd($bb);
        });

        return redirect()->route('sales:index')->with('success', 'Upload has been succesfully');
    }

    public function print(Request $request)
    {
        if ($request->input('print')) {
            $data           = $request->input('print');
            // dd($data);
            $data['sales']  = Sales::whereIn('id',$data)->orderBy('created_at', 'DESC')->get();
            // dd($data['sales']);
            foreach ($data['sales'] as $value) {
                $update = Sales::where('id', $value->id)->first();
                $update->is_print = 1;
                $update->save();    
            }
            
            return view('pages.sales.print', $data);   
        }else{
            return redirect()->route('sales:index')->with('error', 'Please select the data');
        }
    }
}
