<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\User;
use \Cache;
use Carbon\Carbon;
use App\Brand;
use App\Category;
use App\Item;
use App\Stock;
use App\Sales;
use App\SalesDetail;

use DB, Validator, Auth, Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $count  = 0;
        // $user   = User::whereNotNull('last_seen')->get();
        
        // foreach ($user as $data) {
        //     $active = strtotime($data->last_seen);
        //     if (time() - $active < 1 * 60) {
        //         $count = $count + 1;
        //     }  
        // }
        
        $data['brand']          = Brand::whereDate('created_at', Carbon::today())->where('is_published', 1)->count();
        $data['sales']          = Sales::whereDate('created_at', Carbon::today())->where('is_published', 1)->count();
        // $data['sales_detail']   = SalesDetail::whereDate('created_at', Carbon::today())->sum('total');
        $data['sales_detail']   = SalesDetail::where('created_at', '>=', date('Y-m-d').' 00:00:00')->where('created_at', '<=', date('Y-m-d').' 23:59:59')->sum('total');
        $data['item']           = Item::whereDate('created_at', Carbon::today())->where('is_published', 1)->count();
        //dd($data['item']);

        return view('pages.beranda.index', $data, compact('count'));
    }

    public function current()
    {
        $count  = 0;
        $user   = User::whereNotNull('last_active')->get();

        foreach ($user as $data) {
            $active = strtotime($data->last_active);
            if (time() - $active < 1 * 60) {
                $count = $count + 1;
            }  
        }

        return $count;
    }

    public function summary(Request $request){
        //dd('checkpoint');
        
        if ($request->input('_filter') == 1) {
            $data['brand']          = Brand::whereDate('created_at', Carbon::today())->where('is_published', 1)->count();
            $data['sales']          = Sales::whereDate('created_at', Carbon::today())->where('is_published', 1)->count();
            $data['sales_detail']   = SalesDetail::whereDate('created_at', Carbon::today())->sum('total');
            $data['item']           = Item::whereDate('created_at', Carbon::today())->where('is_published', 1)->count();

        }else{
            $data['brand']          = Brand::where('is_published', 1)->count();
            $data['sales']          = Sales::where('is_published', 1)->count();
            $data['sales_detail']   = SalesDetail::sum('total');
            $data['item']           = Item::where('is_published', 1)->count();
        }

        //dd($data['brand'].'-'.$data['sales'].'-'.$data['sales_detail'].'-'.$data['item']);
        
        return response()->json([
            'message' => 'sucess',
            'brand' => number_format($data['brand']),
            'sales' => number_format($data['sales']),
            'sales_detail' => number_format($data['sales_detail']),
            'item' => number_format($data['item'])
        ], 200);
    }
}
