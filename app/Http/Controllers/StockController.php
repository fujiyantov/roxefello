<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Category;
use App\Item;
use App\Stock;
use App\Sales;
use App\SalesDetail;

use DB, Validator, Auth, Session;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->input('_remaining'));
        $item   = $request->input('_item');
        $date   = $request->input('_date');
        $qty    = str_replace(',', '', $request->input('_qty'));
        $cogs   = str_replace(',', '', $request->input('_cogs'));
        $retail = str_replace(',', '', $request->input('_retail'));
        // $remaining  = str_replace(',', '', $request->input('_remaining'));

        // dd($date.'-'.$qty.'-'.$cogs.'-'.$retail);

        $getItem = Item::findOrFail($item);

        if ($getItem->sub != '') {
            $parent = Item::findOrFail($getItem->sub);

            if ($request->input('_id') == 0) {
                $stock  = New Stock;    
                $stock->remaining     = $qty * $getItem->weight;
            }else{
                $stock      = Stock::findOrFail($request->input('_id'));
                $remaining  = str_replace(',', '', $request->input('_remaining'));

                $stock->remaining          = $remaining;
            }
            
            $stock->item          = $item;
            $stock->purchase_date = $date;
            $stock->qty           = $qty;
            $stock->remaining     = $qty * $getItem->weight;
            $stock->cogs          = $cogs;
            $stock->retail        = $retail;
            $stock->created_by    = Auth::id();
            $stock->save();

            // $total_weight   = $stock->remaining;
            // $max = $parent->weight; 
            // $y   = $total_weight/$max;

            // $k = 0;
            // do {

            //     $getStock = Stock::where('item', $parent->id)->where('qty', '>', 0)->where('remaining', '>', 0)->orderBy('created_at', 'ASC')->first();
            //     //dd($getStock);

            //     if ($y > $getStock->qty || $total_weight > $getStock->remaining) {
            //         //dd('TRUE');
                    
            //         $y                   = $y - $getStock->qty ; 
            //         $total_weight        = $total_weight - $getStock->remaining;

            //         $getStock->qty       = 0;
            //         $getStock->remaining = 0;
            //         $getStock->save();

            //     }else{
            //         //dd('FALSE');

            //         $decrement_stock     = $getStock->qty - $y; 
            //         $decrement           = $getStock->remaining - $total_weight;
                    
            //         $getStock->qty       = $decrement_stock;
            //         $getStock->remaining = $decrement;
            //         $getStock->save();

            //         $k++;
            //     }
                
            // } while ($k < 1);

            if ($stock) {
                $stock  = Stock::where('item', $item)->orderBy('created_at', 'DESC')->get();
                return response()->json($stock);
            }else{
                return response()->json('Something wrong!');
            }
        }else{

            if ($request->input('_id') == 0) {
                $stock  = New Stock;
                $stock->remaining     = $qty * $getItem->weight;    
            }else{
                $stock  = Stock::findOrFail($request->input('_id'));
                $remaining  = str_replace(',', '', $request->input('_remaining'));

                $stock->remaining          = $remaining;
            }
            
            $stock->item          = $item;
            $stock->purchase_date = $date;
            $stock->qty           = $qty;
            $stock->cogs          = $cogs;
            $stock->retail        = $retail;
            $stock->created_by    = Auth::id();
            $stock->save();

            if ($stock) {
                $stock  = Stock::where('item', $item)->orderBy('created_at', 'DESC')->get();
                return response()->json($stock);
            }else{
                return response()->json('Something wrong!');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $delete = Stock::find($id)->delete();
        if ($delete) {
            return redirect()->back()->with('success', 'Stock Berhasil dihapus');
        } else {
            return redirect()->back()->with('warning', 'Stock tidak berhasil dihapus');
        }
        
    }

    public function sold(Request $request){
        $item  = $request->get('_item');
        $date  = $request->get('_date');

        $time  = explode('/', $request->get('_date'));
        $month = $time[0];
        $year  = $time[1];

        $data['item']   = Item::findOrFail($item);
        $data['sold']  = SalesDetail::where('sku', $item)
                    ->whereMonth('created_at', $month)
                    ->whereYear('created_at', $year)
                    ->sum('qty');

        $date    = $year."-".$month."-01";
        $data['maxDays'] = date('t',strtotime($date));
        $data['month_name'] = date('M',strtotime($date));
        //dd($data['month']);
        return view('pages.merchandise.sold', $data, compact('month'));
    }

    public function getStatus(){
        // dd('a');
        $item = Item::all();
        return view('status', compact('item'));
    }
}
