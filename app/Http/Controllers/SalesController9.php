<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\SalesImport;
use Carbon\Carbon;
use App\Brand;
use App\Category;
use App\Item;
use App\Stock;
use App\Sales;
use App\SalesDetail;

use DB, Validator, Auth, Session;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Carbon::today());
        $data['sales'] = Sales::where('is_published', 1)->where('created_at', '>=', Carbon::today())->orderBy('created_at', 'DESC')->paginate(20);
        //$data['sales'] = Sales::all();
        //dd($data['sales']);
        return view('pages.sales.index', $data)->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $data['trx_code']   = trx_code(); 
        $share_only         = Item::where('is_published', 1)->where('type', 1)->where('size', 2)->get();
        $count = count($share_only);
        
        if ($count > 0) {
            foreach ($share_only as $value) {
                $res[] = $value->id;
            }    
        }else{
            $res = array(0);
        }


        $data['item']       = Item::where('is_published', 1)->whereNotIn('id', $res)->orderBy('name', 'ASC')->get();

        return view('pages.sales.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->input('customer_address'));
        $rules = [
            'order_number'  => 'required|unique:sales',
            'order_date'  => 'required',
            'shipment'  => 'required',
            'customer_name'  => 'required',
            'customer_address'  => 'required',
            'customer_contact'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Please fix the error(s) below');
        }
        
        /*Order*/
        $order_type         = $request->input('order_type');
        $order_number       = $request->input('order_number');
        $order_date         = $request->input('order_date');
        $shipment           = $request->input('shipment');
        $shipment_type      = $request->input('shipment_type');

        /*Customer*/
        $customer_name      = $request->input('customer_name');
        $customer_contact   = $request->input('customer_contact');
        $customer_address   = $request->input('customer_address');

        //Sales
        $sales  = new Sales;
        $sales->order_type          = 2;
        $sales->order_number        = $order_number;
        $sales->order_date          = $order_date;
        $sales->customer_name       = $customer_name;
        $sales->customer_contact    = $customer_contact;
        $sales->customer_address    = $customer_address;
        $sales->shipment            = $shipment;
        $sales->shipment_type       = $shipment_type;
        $sales->is_published        = 1;
        $sales->created_by          = Auth::id();
        $sales->save();

        /*item*/
        $i = 0;
        //$sales_item      = $request->input('sales_item'); //name item
        $item_id         = $request->input('item_id'); //item id -> looping
        // dd($item_id);
        foreach ($item_id as $data) {
        
            $getItem    = Item::findOrFail($data); //get data item

            if ($getItem->weight != 0) {
                /*cek apakah item termasuk item share atau tidak*/
                if ($getItem->sub != '') {
                    //item share
                    $getStock       = Stock::where('item', $getItem->id)->orderBy('created_at', 'ASC')->first(); //get stock parent
                    // dd($getStock);
                    // dd($getItem->id);

                    $detail = new SalesDetail;
                    $detail->sales              = $sales->id;
                    $detail->sku                = $getItem->id;
                    $detail->merchandise        = $getItem->name;
                    $detail->qty                = $request->input('sales_qty')[$i]; //sales_qty
                    if ($getStock->cogs != '') {
                        $detail->cogs           = $getStock->cogs;    
                    }

                    if ($getStock->retail) {
                        $detail->retail         = $getStock->retail;    
                        $detail->total          = $request->input('sales_qty')[$i] * $getStock->retail;
                    }
                    $detail->created_by         = Auth::id();
                    $detail->save();

                    //increment $i for looping
                    $i++;

                }else{
                    //item full
                    
                    $getStock   = Stock::where('item', $data)->orderBy('created_at', 'ASC')->first();

                    $detail = new SalesDetail;
                    $detail->sales              = $sales->id;
                    $detail->sku                = $getItem->id;
                    $detail->merchandise        = $getItem->name;
                    $detail->qty                = $request->input('sales_qty')[$i]; //sales_qty

                    if ($getStock->cogs != '') {
                        $detail->cogs           = $getStock->cogs;    
                    }

                    if ($getStock->retail) {
                        $detail->retail         = $getStock->retail;    
                        $detail->total          = $request->input('sales_qty')[$i] * $getStock->retail;
                    }

                    $detail->created_by         = Auth::id();
                    $detail->save();

                    //increment $i for looping
                    $i++;
                }
            }else{
                /*cek apakah item termasuk item share atau tidak*/
                if ($getItem->sub != '') {
                    //item share
                    $getStock       = Stock::where('item', $getItem->id)->orderBy('created_at', 'ASC')->first(); //get stock parent

                    $detail = new SalesDetail;
                    $detail->sales              = $sales->id;
                    $detail->sku                = $getItem->id;
                    $detail->merchandise        = $getItem->name;
                    $detail->qty                = $request->input('sales_qty')[$i]; //sales_qty
                    if ($getStock->cogs != '') {
                        $detail->cogs           = $getStock->cogs;    
                    }

                    if ($getStock->retail) {
                        $detail->retail         = $getStock->retail;    
                        $detail->total          = $request->input('sales_qty')[$i] * $getStock->retail;
                    }
                    $detail->created_by         = Auth::id();
                    $detail->save();

                }else{
                    //item full
                    
                    $getStock   = Stock::where('item', $data)->orderBy('created_at', 'ASC')->first();

                    $detail = new SalesDetail;
                    $detail->sales              = $sales->id;
                    $detail->sku                = $getItem->id;
                    $detail->merchandise        = $getItem->name;
                    $detail->qty                = $request->input('sales_qty')[$i]; //sales_qty

                    if ($getStock->cogs != '') {
                        $detail->cogs           = $getStock->cogs;    
                    }

                    if ($getStock->retail) {
                        $detail->retail         = $getStock->retail;    
                        $detail->total          = $request->input('sales_qty')[$i] * $getStock->retail;
                    }

                    $detail->created_by         = Auth::id();
                    $detail->save();

                    //increment $i for looping
                    $i++;
                }
            }
        }
        //dd('Development');
        if ($detail) {
            return redirect(route('sales:index'))->with('success', 'Sales Saved Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please contact developer');
        }

        // echo 'end looping';
        // exit;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['sales'] = Sales::where('is_published', 1)->where('id', $id)->first();
        $data['item']  = SalesDetail::where('sales', $data['sales']->id)->get();
        //dd($data['sales']->id);
        return view('pages.sales.show', $data)->with('i');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $share_only         = Item::where('is_published', 1)->where('type', 1)->where('size', 2)->get();
        $count = count($share_only);
        
        if ($count > 0) {
            foreach ($share_only as $value) {
                $res[] = $value->id;
            }    
        }else{
            $res = array(0);
        }

        $data['item']   = Item::where('is_published', 1)->whereNotIn('id', $res)->orderBy('name', 'ASC')->get();
        $data['sales']  = Sales::findOrFail($id);
        $data['detail'] = SalesDetail::where('sales', $id)->get();
        //dd($data['detail']);
        
        return view('pages.sales.edit', $data)->with('i');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'order_date'  => 'required',
            'shipment'  => 'required',
            'customer_name'  => 'required',
            'customer_address'  => 'required',
            'customer_contact'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Please fix the error(s) below');
        }
        
        /*Order*/
        $order_type         = $request->input('order_type');
        $order_number       = $request->input('order_number');
        $order_date         = $request->input('order_date');
        $shipment           = $request->input('shipment');
        $shipment_type      = $request->input('shipment_type');

        /*Customer*/
        $customer_name      = $request->input('customer_name');
        $customer_contact   = $request->input('customer_contact');
        $customer_address   = $request->input('customer_address');

        //Sales
        $sales  = Sales::findOrFail($id);
        $sales->order_type          = $order_type;
        $sales->order_number        = $order_number;
        $sales->order_date          = $order_date;
        $sales->customer_name       = $customer_name;
        $sales->customer_contact    = $customer_contact;
        $sales->customer_address    = $customer_address;
        $sales->shipment            = $shipment;
        $sales->shipment_type       = $shipment_type;
        $sales->is_published        = 1;
        $sales->created_by          = Auth::id();
        $sales->save();

        $item   = $request->input('item'); //item id -> looping
        $qty    = $request->input('qty'); 
        $cogs   = $request->input('cogs'); 
        $retail = $request->input('retail'); 

        $i = 0;
        foreach ($item as $data) {
            $getItem    = Item::findOrFail($data);

            $sales = SalesDetail::where('sales', $id)->where('sku', $data)->first();
            $sales->cogs    = $cogs[$i];
            $sales->retail  = $retail[$i];
            $sales->total   = $qty[$i] * $retail[$i];
            $sales->save();

            $stock = Stock::where('item', $data)->where('cogs', 0)->where('retail', 0)->orderBy('created_at', 'DESC')->first();
            $stock->cogs    = $cogs[$i];
            $stock->retail  = $retail[$i];
            $stock->save();
        }

        if ($stock) {
            return redirect(route('sales:index'))->with('success', 'Sales Updated Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please contact developer');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function excel(Request $request)
    {
        // dd($request->file('excel'));

        $excelFile  = $request->file('excel');
        $number                 = 0;
        $item_tidak_terdaftar   = 0;
        $stock_habis            = 0;
        Excel::load($excelFile, function($reader) {

            $number                 = 0;
            $item_tidak_terdaftar   = 0;
            $stock_habis            = 0;

            foreach ($reader->toArray() as $row) {
                /*debug*/
                // $bb[] = $row;

                // cek item
                $item_name  = $row['nama_produk'].' '.$row['nama_variasi'];
                // $slug_item  = slug($item_name);
                // $get_item   = Item::where('slug', $slug_item)->first();
                $get_item   = Item::where('name', $item_name)->first();

                // dd($get_item);
                if (@count($get_item) > 0) {
                    $getStock   = Stock::where('item', $get_item->id)->orderBy('created_at', 'ASC')->first();
                    // dd($getStock);
                    if (@count($getStock) > 0) {
                        // dd('ayam');
                        if ($row['no._pesanan'] != NULL) {
                            Session::put('no_pesanan', $row['no._pesanan']);
                            Session::put('waktu_pembayaran_dilakukan', $row['waktu_pembayaran_dilakukan']);
                            Session::put('nama_penerima', $row['nama_penerima']);
                            Session::put('no_telepon', $row['no._telepon']);
                            Session::put('alamat_pengiriman', $row['alamat_pengiriman']);
                            Session::put('opsi_pengiriman', $row['opsi_pengiriman']);
                            
                            // dd('ayam');
                            $check = Sales::where('order_number', $row['no._pesanan'])->first();

                            if (@count($check) > 0) {
                                // dd('ayam');
                                $check_detail = SalesDetail::where('sales', $check->id)->where('sku', $get_item->id)->first();
                                if (@count($check_detail) > 0) {
                                    $number++;
                                    Session::put('check_excel', $number);
                                    
                                    $order[] = $check->order_number.'-'.$check_detail->sku;   
                                }else{

                                    $item_name = $row['nama_produk'].' '.$row['nama_variasi'];
                                    $slug_item = slug($item_name);
                                    //get item
                                    $get_item  = Item::where('slug', $slug_item)->first();

                                    $detail = new SalesDetail;
                                    $detail->sales       = $check->id;
                                    $detail->sku         = $get_item->id;
                                    $detail->merchandise = $row['nama_produk'];
                                    $detail->qty         = round($row['jumlah']);
                                    $detail->cogs        = preg_replace("/[^0-9]/", "", $row['harga_sebelum_diskon'] );
                                    $detail->retail      = preg_replace("/[^0-9]/", "", $row['harga_setelah_diskon'] );
                                    $detail->total       = preg_replace("/[^0-9]/", "", $row['total_harga_produk'] );
                                    $detail->created_by  = Auth::id();
                                    $detail->save();
                                    
                                    self::ciat($get_item, $detail);   
                                }
                            }else{
                                $create = new Sales;
                                $create->order_type         = 1;
                                $create->order_number       = $row['no._pesanan'];
                                $create->order_date         = $row['waktu_pembayaran_dilakukan'];
                                $create->customer_name      = $row['nama_penerima'];
                                $create->customer_contact   = $row['no._telepon'];
                                $create->customer_address   = $row['alamat_pengiriman'];
                                $create->shipment           = $row['opsi_pengiriman'];
                                $create->is_published       = 1;
                                $create->is_accepted        = 1;
                                $create->created_by         = Auth::id();
                                $create->save();

                                Session::put('sales_id', $create->id);
                                
                                $item_name = $row['nama_produk'].' '.$row['nama_variasi'];
                                // $slug_item = slug($item_name);
                                //get item
                                // $get_item  = Item::where('slug', $slug_item)->first();
                                $get_item   = Item::where('name', $item_name)->first();

                                $detail = new SalesDetail;
                                $detail->sales       = Session::get('sales_id');
                                $detail->sku         = $get_item->id;
                                $detail->merchandise = $row['nama_produk'];
                                $detail->qty         = round($row['jumlah']);
                                $detail->cogs        = preg_replace("/[^0-9]/", "", $row['harga_sebelum_diskon'] );
                                $detail->retail      = preg_replace("/[^0-9]/", "", $row['harga_setelah_diskon'] );
                                $detail->total       = preg_replace("/[^0-9]/", "", $row['total_harga_produk'] );
                                $detail->created_by  = Auth::id();
                                $detail->save();

                                self::ciat($get_item, $detail);
                               
                            }
                        }else{
                            $check = Sales::where('order_number', Session::get('no_pesanan'))->first();
                            if (@count($check) > 0) {
                                $sales_id = $check->id;
                            }else{
                                $create = new Sales;
                                $create->order_type         = 1;
                                $create->order_number       = Session::get('no_pesanan');
                                $create->order_date         = Session::get('waktu_pembayaran_dilakukan');
                                $create->customer_name      = Session::get('nama_penerima');
                                $create->customer_contact   = Session::get('no_telepon');
                                $create->customer_address   = Session::get('alamat_pengiriman');
                                $create->shipment           = Session::get('opsi_pengiriman');
                                $create->is_published       = 1;
                                $create->is_accepted        = 1;
                                $create->created_by         = Auth::id();
                                $create->save();

                                $sales_id = $create->id;
                            }

                            //search item from index nama_produk and nama_variasi
                            $item_name = $row['nama_produk'].' '.$row['nama_variasi'];
                            // dd($item_name);
                            // $slug_item = slug($item_name);
                            //get item
                            // $get_item  = Item::where('slug', $slug_item)->first();
                            $get_item   = Item::where('name', $item_name)->first();

                            $detail = new SalesDetail;
                            // $detail->sales       = Session::get('sales_id');
                            $detail->sales       = $sales_id;
                            $detail->sku         = $get_item->id;
                            $detail->merchandise = $row['nama_produk'];
                            $detail->qty         = round($row['jumlah']);
                            $detail->cogs        = preg_replace("/[^0-9]/", "", $row['harga_sebelum_diskon'] );
                            $detail->retail      = preg_replace("/[^0-9]/", "", $row['harga_setelah_diskon'] );
                            $detail->total       = preg_replace("/[^0-9]/", "", $row['total_harga_produk'] );
                            $detail->created_by  = Auth::id();
                            $detail->save();

                            self::ciat($get_item, $detail);  
                        }
                    }else{
                        // stock habis
                        $stock_habis++;
                        Session::put('check_stock', $stock_habis);

                        $stock_kosong[] = $item_name;

                    }
                }else{
                    // item tidak terdaftar
                    $item_tidak_terdaftar++;
                    Session::put('check_item', $item_tidak_terdaftar);

                    $item_kosong[] = $item_name;
                }
                    
            }

            // dd($order);
            if (isset($order)) {
                Session::put('values', $order);    
            }

            if (isset($stock_kosong)) {
                Session::put('values_stock_kosong', $stock_kosong);    
            }

            if (isset($item_kosong)) {
                Session::put('values_item_kosong', $item_kosong);    
            }

            /* debug */
            // dd($bb);

        });
        
        if (Session::get('check_excel') != 0 || Session::get('check_stock') != 0 || Session::get('check_item') != 0) {
            return redirect()->back()->with('error', 'Something Wrong!');
        }else{
            return redirect()->route('sales:index')->with('success', 'Upload has been succesfully');
        }
    }

    public function ciat($get_item, $detail){

        if ($get_item->weight != 0) {

            self::weigtNotNull($get_item, $detail);

        }else{
            // weight == 0
            self::weigtIsNull($get_item, $detail); 
        }    
    }

    public function weigtNotNull($get_item, $detail){
        $total_weight   = $get_item->weight * $detail->qty;
        $max = $get_item->weight; 
        $y   = $total_weight/$max;

        if ($get_item->sub != '') {
            $item = $get_item->sub;
        }else{
            $item = $get_item->id;
        }

        $k = 0;

        do {
            //get stock parent
            $getStock = Stock::where('item', $item)->where('remaining', '>', 0)->orderBy('created_at', 'ASC')->first();

            // if ($y > $getStock->qty || $total_weight > $getStock->remaining) {
            if ($total_weight > $getStock->remaining) {
                
                $y                   = $y - $getStock->qty ; 
                $total_weight        = $total_weight - $getStock->remaining;

                $getStock->qty       = ($y <= 0)? 0 : $y;
                $getStock->remaining = ($total_weight <= 0)? 0 : $total_weight;
                $getStock->save();

            }else{

                $parent = Item::where('id', $getStock->item)->first();
                if ($total_weight >= $parent->weight) {
                    $y = $y;
                }else{
                    $y = 0;
                }
                // $y = ($y > 0 )? $y : 0 ;

                $decrement_stock     = $getStock->qty - $y; 
                $decrement           = $getStock->remaining - $total_weight;
                
                $getStock->qty       = ($decrement_stock <= 0)? 0 : $decrement_stock;
                $getStock->remaining = ($decrement <= 0)? 0 : $decrement;
                $getStock->save();

                $k++;
            }
            
        } while ($k < 1); 
    }

    public function weigtIsNull($get_item, $detail){
        $y = $detail->qty;
        $k = 0;

        if ($get_item->sub != '') {
            $item = $get_item->sub;
        }else{
            $item = $get_item->id;
        }

        do {

            $getStock = Stock::where('item', $item)->orderBy('created_at', 'ASC')->first();

            if ($y > $getStock->qty) {
                
                $y                   = $y - $getStock->qty ; 

                $getStock->qty       = ($y <= 0)? 0 : $y;
                $getStock->save();

            }else{

                $decrement_stock     = $getStock->qty - $y; 
                
                $getStock->qty       = ($decrement_stock <= 0)? 0 : $decrement_stock;
                $getStock->save();

                $k++;
            }
            
        } while ($k < 1);
    }

    public function print(Request $request)
    {
        if ($request->input('print')) {
            $data           = $request->input('print');
            // dd($data);
            $data['sales']  = Sales::whereIn('id',$data)->orderBy('created_at', 'DESC')->get();
            // dd($data['sales']);
            foreach ($data['sales'] as $value) {
                $update = Sales::where('id', $value->id)->first();
                $update->is_print = 1;
                $update->save();    
            }
            
            return view('pages.sales.print', $data);   
        }else{
            return redirect()->route('sales:index')->with('error', 'Please select the data');
        }
    }

    public function filter(Request $request)
    {
        $data['order_type']         = $request->input('_type');
        //dd($data['type']);
        $data['order_number']       = $request->input('_order_number');
        $data['start_at']           = $request->input('_start_at');
        $data['end_at']             = $request->input('_end_at');
        $data['customer_name']      = $request->input('_customer_name');
        $data['customer_contact']   = $request->input('_customer_contact');
        $data['print']              = $request->input('_print');

        $data['sales']  = Sales::filter($data);
        //dd($data['sales']);

        $data           = collect($data['sales']);
        $sales          = collect(json_decode($data));
        $currentPage    = LengthAwarePaginator::resolveCurrentPage();
        $perPage        = 20;
        $currentResults = $sales->slice(($currentPage - 1) * $perPage, $perPage)->all();
        
        $paginate       =  new LengthAwarePaginator($currentResults, $sales->count(), $perPage);
        $paginate       = $paginate->setPath('');
        /*$data['sales']  = $paginate;
        dd($data['sales']);*/

        $sales  = $paginate;
        // dd($sales);        
        return view('pages.sales.filter', compact('sales'))->with('i');
    }

    public function getDadakan(Request $request)
    {
        return view('dadakan');
    }

    public function postDadakan(Request $request)
    {
        //dd($request->file('excel'));

        $excelFile  = $request->file('excel');
        $number     = 0;

        Excel::load($excelFile, function($reader) {
            // foreach ($reader->toArray() as $row) {
            //     /*debug*/
            //     $bb[] = $row;
            // }

            // dd($bb);
            // asort($bb);

            // foreach ($bb as $row) {
            //     $cc[] = $row;
               
            // }

            // dd($cc);

            foreach ($reader->toArray() as $row) {
                /*debug*/
                $bb[] = $row;
                    // dd($row['sisashare']);
                    $brand = Brand::where('name', ucwords($row['brand']))->first();
                    if (@count($brand) < 1) {
                        $brand = new Brand;
                        $brand->name         = ucwords($row['brand']);
                        $brand->code         = strtoupper(substr(($row['brand']), 0, 3));
                        $brand->is_published = 1;
                        $brand->created_by   = 1;
                        $brand->save();    
                    }

                    $category = Category::where('name', ucwords($row['category']))->first();
                    if (@count($category) < 1) {
                        $category = new Category;
                        $category->name         = ucwords($row['category']);
                        $category->is_published = 1;
                        $category->created_by   = 1;
                        $category->save();    
                    }

                    // if (strtoupper($row['size']) == 'FULL') {
                    //     $size = 'Full';
                    // }elseif(strtoupper($row['size']) == 'SHARE'){
                    //     $size = 'Share';
                    // }else{
                    //     $size = '';
                    // }

                    $unit       = preg_replace('/[^a-zA-Z]/', '', $row['weight']);
                    // $size       = (strtoupper($row['size']) == 'FULL')? 'Full' : 'Share';
                    $size       = (strtoupper($row['size']) == 'SHARE')? 'Share' : 'Full';
                    $color      = (strtoupper($row['color']) != NULL)? ucwords($row['color']) : '';

                    $clWeight   = preg_replace("/[^0-9]/","",$row['weight']);
                    $weight     = ($clWeight != '') ? $clWeight : 0 ;

                    $name_item  = $brand->name.' '.ucwords($row['item']).' '.$color.' '.$category->name.' '.$size.' '.$weight.' '.$unit;
                    $slug       = slug($name_item);

                    $item                   = New Item;
                    $item->type             = ((string)$row['sisashare'] == '0')? '2' : '1';
                    $item->brand_id         = $brand->id;
                    $item->category_id      = $category->id;
                    // $item->name             = $name_item;
                    $item->name             = $row['long_description'];
                    // $item->slug             = $slug;
                    $item->slug             = slug($row['long_description']);
                    $item->sku              = ucwords($row['sku']);
                    $item->size             = ($size == 'Full')? '1' : '2';
                    $item->weight           = round($weight);
                    $item->item             = ucwords($row['item']);
                    $item->color            = $color;
                    $item->unit             = $unit;
                    $item->description      = 'Nothing';
                    $item->is_published     = 1;
                    $item->created_by       = 1;
                    $item->save();

                    $cogs   = 0;
                    $retail = 0;

                    if ($row['cogs'] != NULL) {
                        $cogs = preg_replace("/[^0-9]/","",$row['cogs']);    
                    }

                    if ($row['price'] != NULL) {
                        $retail = preg_replace("/[^0-9]/","",$row['price']);
                    }
                    
                    $share = preg_replace("/[^0-9]/","",$row['sisashare']);
                    $sisa  = (int)$share;

                    $stock  = New Stock;
                    $stock->item          = $item->id;
                    $stock->purchase_date = date('Y-m-d 00:00:00');
                    $stock->qty           = ($row['stock_19feb'] != '')? $row['stock_19feb'] : 0;
                    $stock->remaining     = ($row['stock_19feb'] * $item->weight) + $sisa;
                    $stock->cogs          = (is_numeric($cogs)) ? $cogs : 0 ;
                    $stock->retail        = (is_numeric($retail)) ? $retail : 0 ;
                    $stock->created_by    = 1;
                    $stock->save();
            }
            /* debug */
            //dd($bb);
        });
        //dd('checkpoint');
        return 'oke';
    }

    public function parentChildDadakan(Request $request)
    {
        $item = Item::all();
        // $item = Item::where('id', '>', '372')->get();

        foreach ($item as $data) {
            $getSKU = preg_replace("/[^0-9]/","",$data->sku);
            // dd($getSKU);
            if ($data->size == 2 && $data->sku != $getSKU) {
                $parent      = Item::where('sku', $getSKU)->first();
                // dd($parent);
                $update      = Item::findOrFail($data->id);
                // $update->sub = (int)$getSKU;
                $update->sub = $parent->id;
                $update->save();
            }
        }

        return 'oke';
    }

    public function check(Request $request)
    {
        // return $request->input('_id')
        $data['sales']  = Sales::find($request->input('_id'));
        // return $data['sales'];
        $data['item']   = SalesDetail::where('sales', $data['sales']->id)->get();
        // return $data['item'];

        return view('pages.sales.modal', $data);
    }

    public function accepted(Request $request)
    {
        // dd($request->input('sales_id'));

        $sales  = Sales::find($request->input('sales_id'));
        // dd($sales);
        $item   = SalesDetail::where('sales', $sales->id)->get();
        // dd($item);
        foreach ($item as $detail) {

            $get_item = Item::where('id', $detail->sku)->first();

            if ($get_item->weight != 0) {

                self::weigtNotNull($get_item, $detail);

            }else{
                // weight == 0
                self::weigtIsNull($get_item, $detail); 
            } 
        }

        $sales->is_accepted = 1;
        $sales->save();

        return redirect()->back()->with('success', 'Your accepted is succesfully');

        // dd('checkpoint');
        // return view('pages.sales.modal', $data);
    }
}
