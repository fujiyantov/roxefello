<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Brand;
use App\Category;
use App\Item;
use App\Stock;
use App\Sales;
use App\SalesDetail;

use DB, Validator, Auth, Session;

class SoldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sold']   = SalesDetail::whereNotNull('sku')
                    ->whereMonth('created_at', Carbon::now()->month)
                    ->whereYear('created_at', Carbon::now()->year)
                    ->distinct('sku')
                    ->select('sku')
                    ->get();
        
        Session::put('monthly', 1);
        Session::put('month', Carbon::now()->month);
        Session::put('year', Carbon::now()->year);
        //dd($data['sold']);

        return view('pages.report.sold.index', $data)->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function qty(Request $request)
    {
        $time  = explode('/', $request->get('_date'));
        $month = $time[0];
        $year  = $time[1];

        $data['sold']   = SalesDetail::whereNotNull('sku')
                    ->whereMonth('created_at', $month)
                    ->whereYear('created_at', $year)
                    ->distinct('sku')
                    ->select('sku')
                    ->get();
        //dd($data['sold']);
        
        Session::put('monthly', 1);
        Session::put('month', Carbon::now()->month);
        Session::put('year', Carbon::now()->year);

        return view('pages.report.sold.qty', $data)->with('i');
    }
}
