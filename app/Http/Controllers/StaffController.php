<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\SalesImport;
use App\Brand;
use App\Category;
use App\Item;
use App\Stock;
use App\Sales;
use App\SalesDetail;
use App\User;
use App\Role;

use DB, Validator, Auth, Session;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['staff'] = User::all();
        return view('pages.staff.index', $data)->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( Auth::user()->role == 1 ){
            $role = Role::all();
            return view('pages.staff.create', compact('role'));
        }else{
            return redirect()->route('welcome');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Please fix the error(s) below');
        }

        $staff                 = New User;
        $staff->name           = $request->input('name');
        $staff->role           = $request->input('permisson');
        $staff->email          = $request->input('email');
        $staff->password       = Hash::make($request->input('password'));
        $staff->save();

        // $staff->roles()->sync($request->input('permisson'));

        $staff->attachRole($request->input('permisson'));

        if ($staff) {
            return redirect(route('staff:index'))->with('success', 'Staff Saved Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please try again');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('pages.staff.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $param = substr($id, 10, 1);
        // dd($param);
        // dd(Session::get('key'.substr($id, 10, 1)));
        // $id = base64_decode($id);
        $id = base64_decode(Session::get('key'.substr($id, 10, 1)));
        // dd($id);
        $data['role'] = Role::all();
        $data['staff']  = User::where(DB::raw('LEFT(MD5(id), 10)'), $id)->first();
        // dd($data['staff']);
        return view('pages.staff.edit', $data, compact('param'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = base64_decode(Session::get('key'.substr($id, 10, 1)));
        // $id = base64_decode($id);

        $rules = [
            'name' => 'required|string|max:255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Please fix the error(s) below');
        }
        $staff                 = User::where(DB::raw('LEFT(MD5(id), 10)'), $id)->first();
        // dd($staff);
        $staff->name           = $request->input('name');
        if( Auth::user()->role == 1 ){
            $staff->role           = $request->input('permisson');
        }else{
            $staff->role           = Auth::user()->role;
        }
        
        $staff->email          = $request->input('email');

        if ($request->input('password')) {
            $staff->password       = Hash::make($request->input('password'));    
        }
        
        $staff->save();

        $staff->roles()->sync($request->input('permisson'));

        // $staff->attachRole($request->input('permisson'));

        if ($staff) {
            return redirect()->back()->with('success', 'Staff Saved Successfully');
            // return redirect(route('staff:index'))->with('success', 'Staff Saved Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please try again');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function backup($host = 'localhost',$user = 'root',$pass= 'root',$name= 'roxefello',$tables = '*'){
        $return='';
        $link = mysqli_connect($host, $user, $pass, $name);
 
        if($tables == '*')
        {
            $tables = array();
            $result = mysqli_query($link,'SHOW TABLES');
            while($row = mysqli_fetch_row($result))
            {
                $tables[] = $row[0];
            }
        }
        else
        {
            $tables = is_array($tables) ? $tables : explode(',',$tables);
        }
       
        //cycle through
        foreach($tables as $table)
        {
            $result = mysqli_query($link,'SELECT * FROM '.$table);
            $num_fields = mysqli_num_fields($result);
           
            $return.= 'DROP TABLE IF EXISTS '.$table.';';
            $row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
 
            $return.= "\n\n".$row2[1].";\n\n";
           
            for ($i = 0; $i < $num_fields; $i++)
            {
                while($row = mysqli_fetch_row($result))
                {
                    $return.= 'INSERT INTO '.$table.' VALUES(';
                    for($j=0; $j < $num_fields; $j++)
                    {
                        $row[$j] = addslashes($row[$j]);
                        if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                        if ($j < ($num_fields-1)) { $return.= ','; }
                    }
                    $return.= ");\n";
                }
            }
            $return.="\n\n\n";
        }
       
        // $handle = fopen('#dbbackup/roxefello-'.date("Y-m-d").'.sql','w+');
        // fwrite($handle,$return);
        // fclose($handle);
 
        //add below code to download it as a sql file
        Header('Content-type: application/sql');
        Header('Content-Disposition: attachment; filename=roxefello-'.date("Y-m-d").'.sql','w+');
        echo $return;

        //return view('user.exportSuccess', $data);
    }

    function restore(){
        dd('a');
    }    
}
