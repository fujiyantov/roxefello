<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Brand;
use App\Category;
use App\Item;
use App\Stock;
use App\Sales;
use App\SalesDetail;

use DB, Validator, Auth, Session;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['item'] = Item::orderBy('created_at', 'DESC')->whereNull('sub')->get();
        $data['item']       = Item::orderBy('created_at', 'DESC')->paginate(20);
        $data['brand']      = Brand::orderBy('name', 'ASC')->get();
        $data['category']   = Category::orderBy('name', 'ASC')->get();
        return view('pages.merchandise.index', $data)->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $data['item']       = Item::where('is_published', 1)->where('type', 1)->where('size', 1)->whereNull('sub')->orderBy('name', 'ASC')->get();
        // $data['item']       = Item::find(1793);
        // dd($data['item']);
        $data['brand']      = Brand::where('is_published', 1)->orderBy('name', 'ASC')->get();
        $data['category']   = Category::where('is_published', 1)->orderBy('name', 'ASC')->get();
        return view('pages.merchandise.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->input('size'));
        if ($request->input('type') == 1) {

            $rules = [
                // 'name' => 'required|min:3|max:255|unique:merchandises',
                'sku' => 'required|min:3|max:255|unique:merchandises',
                'size' => 'required',
                // 'weight' => 'required',
                'item' => 'required',
                'unit' => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput()
                    ->with('error', 'Please fix the error(s) below');
            }

            $type_of = $request->input('size');
            if ($type_of == 1) {
                //full only
                //dd('1');

                $brand      = Brand::findOrFail($request->input('brand'));
                $category   = Category::findOrFail($request->input('category'));

                $unit       = ($request->input('unit') == 1)? 'gr' : 'ml';
                $name_item  = $brand->name.' '.$request->input('item').' '.$request->input('color').' '.$category->name.' Full '.$request->input('weight').' '.$unit;
                $slug       = slug($name_item);

                $item                   = New Item;
                $item->type             = $request->input('type');
                $item->brand_id         = $request->input('brand');
                $item->category_id      = $request->input('category');
                $item->name             = $name_item;
                $item->slug             = $slug;
                $item->sku              = $request->input('sku');
                $item->size             = $request->input('size');
                $item->weight           = $request->input('weight');
                $item->item             = $request->input('item');
                $item->color            = $request->input('color');
                $item->unit             = $request->input('unit');
                $item->description      = $request->input('description');
                $item->created_by       = Auth::id();
                $item->is_published     = 1;
                $item->save();

                if ($item) {
                    return redirect(route('item:show', [$item->id, slug($item->name)]))->with('success', 'Item Saved Successfully');
                }else{
                    return redirect()->back()->with('error', 'Something wrong, Please try again');
                }

            }else{
                //share only
                //dd('2');
                $brand      = Brand::findOrFail($request->input('brand'));
                $category   = Category::findOrFail($request->input('category'));

                $unit       = ($request->input('unit') == 1)? 'gr' : 'ml';
                $name_item  = $brand->name.' '.$request->input('item').' '.$request->input('color').' '.$category->name.' Full '.$request->input('weight').' '.$unit;
                $slug       = slug($name_item);

                $item                   = New Item;
                $item->type             = $request->input('type');
                $item->brand_id         = $request->input('brand');
                $item->category_id      = $request->input('category');
                $item->name             = $name_item;
                $item->slug             = $slug;
                $item->sku              = $request->input('sku');
                $item->size             = $request->input('size');
                $item->weight           = $request->input('weight');
                $item->item             = $request->input('item');
                $item->color            = $request->input('color');
                $item->unit             = $request->input('unit');
                $item->description      = $request->input('description');
                $item->created_by       = Auth::id();
                $item->is_published     = 1;
                $item->save();

                $share_weight   = $request->input('share_weight_only');

                $i = 0;
                foreach ($share_weight as $weight) {
                    // dd($weight);
                    $unit       = ($request->input('unit') == 1)? 'gr' : 'ml';
                    $name_item  = $brand->name.' '.$request->input('item').' '.$request->input('color').' '.$category->name.' Share '.$weight.' '.$unit;
                    $slug       = slug($name_item);
                    // dd($name_item);
                    $item                   = New Item;
                    $item->type             = $request->input('type');
                    $item->brand_id         = $request->input('brand');
                    $item->category_id      = $request->input('category');
                    $item->name             = $name_item;
                    $item->slug             = $slug;
                    $item->sku              = $request->input('sku').'-'.$weight;
                    $item->size             = $request->input('size');
                    $item->weight           = $weight;
                    $item->item             = $request->input('item');
                    $item->color            = $request->input('color');
                    $item->unit             = $request->input('unit');
                    $item->description      = $request->input('description');
                    $item->created_by       = Auth::id();
                    $item->is_published     = 1;
                    $item->save();

                    $i++;
                }

                if ($item) {
                    // return redirect(route('item:index'))->with('success', 'Share Item Successfully');
                    return redirect(route('item:show', [$item->id, slug($item->name)]))->with('success', 'Item Saved Successfully');
                }else{
                    return redirect()->back()->with('error', 'Something wrong, Please try again');
                }
            }
        }else{
            //dd('share');
            //dd($request->input('share_retail'));
            $getItem = Item::findOrFail($request->input('share_item'));
            // dd($getItem);
            
            $brand      = Brand::findOrFail($getItem->brand_id);
            $category   = Category::findOrFail($getItem->category_id);

            $share_weight   = $request->input('share_weight');
            // $share_cogs     = $request->input('share_cogs');
            // $share_retail   = $request->input('share_retail');
            $i = 0;
            foreach ($share_weight as $weight) {
                // dd($weight);
                $unit       = ($getItem->unit == 1)? 'gr' : 'ml';
                $name_item  = $brand->name.' '.$getItem->item.' '.$getItem->color.' '.$category->name.' Share '.$weight.' '.$unit;
                $slug       = slug($name_item);
                // dd($name_item);
                $item                   = New Item;
                $item->type             = $request->input('type');
                $item->brand_id         = $getItem->brand_id;
                $item->category_id      = $getItem->category_id;
                $item->sub              = $getItem->id;
                $item->name             = $name_item;
                $item->slug             = $slug;
                $item->sku              = $getItem->sku.'-'.$weight;
                $item->size             = '2';
                $item->weight           = $weight;
                $item->item             = $getItem->item;
                $item->color            = $getItem->color;
                $item->unit             = $getItem->unit;
                $item->description      = $getItem->description;
                $item->created_by       = Auth::id();
                $item->is_published     = 1;
                $item->save();

                // $stock  = New Stock;
                // $stock->item          = $item->id;
                // $stock->purchase_date = date('Y-m-d');
                // $stock->qty           = 0;
                // $stock->remaining     = 0 * $item->weight;
                // $stock->cogs          = $share_cogs[$i];
                // $stock->retail        = $share_retail[$i];
                // $stock->created_by    = Auth::id();
                // $stock->save();

                $i++;
            }

            if ($item) {
                return redirect(route('item:show', [$item->id, slug($item->name)]))->with('success', 'Item Saved Successfully');
                // return redirect(route('item:index'))->with('success', 'Share Item Successfully');
            }else{
                return redirect()->back()->with('error', 'Something wrong, Please try again');
            }
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['item']   = Item::findOrFail($id);
        $data['parent']   = Item::where('size', 1)->get();
        $data['brand']   = Brand::where('is_published', 1)->get();
        $data['category']   = Category::where('is_published', 1)->get();
        $data['stock']  = Stock::where('item', $id)->orderBy('created_at', 'DESC')->paginate(5);
        $data['sold']   = SalesDetail::where('sku', $id)
                    ->whereMonth('created_at', Carbon::now()->month)
                    ->whereYear('created_at', Carbon::now()->year)
                    ->sum('qty');

        $data['maxDays']=date('t');
        $data['month']  =date('m');
        $data['month_name']  =date('M');

        return view('pages.merchandise.show', $data)->with('i');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['item']  = Item::findOrFail($id);
        $data['full']  = Item::whereIsNull('sub')->get();
        // dd($data['full']);
        return view('pages.merchandise.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $rules = [
            // 'sku' => 'required|min:3|max:255',
            // 'size' => 'required',
            // 'item' => 'required',
            // 'unit' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Please fix the error(s) below');
        }

        $item                 = Item::findOrFail($request->input('id'));
        $item->type           = $request->input('type');
        $item->sub            = $request->input('parent');
        $item->name           = $request->input('name');
        $item->category_id    = $request->input('category');
        $item->brand_id       = $request->input('brand'); 
        $item->sku            = $request->input('sku');
        $item->size           = $request->input('size');
        $item->weight         = $request->input('weight');
        $item->color          = $request->input('color');
        $item->description    = $request->input('description');
        $item->save();

        if ($item) {
            return redirect(route('item:show', [$item->id, slug($item->name)]))->with('success', 'Item Updated Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please try again');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand                 = Item::findOrFail($id);
        $brand->updated_by     = Auth::id();
        $brand->is_published   = 0;
        $brand->save();

        if ($brand) {
            return redirect(route('item:index'))->with('success', 'Brand Unpublish Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please try again');
        }
    }

    public function actived($id)
    {
        $brand                 = Item::findOrFail($id);
        $brand->updated_by     = Auth::id();
        $brand->is_published   = 1;
        $brand->save();

        if ($brand) {
            return redirect(route('item:index'))->with('success', 'Brand Publish Successfully');
        }else{
            return redirect()->back()->with('error', 'Something wrong, Please try again');
        }
    }

    public function brand_code(Request $request){
        $brand_id   = $request->input('_brand');
        $brand      = Brand::findOrFail($brand_id);
        $check      = Item::where('brand_id', $brand->id)->whereNull('sub')->count();
        //dd($check);
        $number     = str_pad($check+1, 6,'0', STR_PAD_LEFT);
        $code       = $brand->code.$number;

        return response()->json($code, 200);
    }

    public function filter(Request $request)
    {

        $data['_type']       = $request->input('_type');
        $data['_name']       = $request->input('_name');
        $data['_sku']        = $request->input('_sku');
        $data['_size']       = $request->input('_size');
        $data['_brand']      = $request->input('_brand');
        $data['_category']   = $request->input('_category');
        $data['_max']        = $request->input('_max');
        $data['_min']        = $request->input('_min');

        $data['item']  = Item::filter($data);

        foreach ($data['item'] as $value) {
            $get[] = $value->id;
        }
        // return $get;
        if (isset($get)) {
            // $data['item']       = Item::whereIn('id', $get)->orderBy('name', 'ASC')->paginate();   
            $data['item']       = Item::whereIn('id', $get)->orderBy('name', 'ASC')->get();
            $data['no_page']    = 1;   
        }

        return view('pages.merchandise.filter', $data)->with('i');

    }
}
