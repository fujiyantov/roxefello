<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Support\Facades\Redis;
use \Cache;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function lastActive() {
       $redis = Redis::connection();
       return $redis->get('last_active_' . $this->id);        
    }

    public function currentUser() {
       // $active = strtotime($this->last_seen);
       // if (time() - $active > 1 * 60) {
       //     // 15 mins has passed
       //     return 
       // }

       // if (date("Y-m-d", $dbdate) != date("Y-m-d")) {
       //     // different date
       // }
       
       //return $redis->get('last_seen_' . $this->id);        
    }

    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

    public static function count()
    {
        return Cache::remember('count_user', 1, function () {
            return static::query()->count();
        });
    }
}
