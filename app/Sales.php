<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Sales extends Model
{
   protected $table = 'sales';
   public $dates = ['created_at', 'updated_at'];

   protected $fillable = [
       'order_number',
   ];

   public function createdBy(){
   	return $this->belongsTo('App\User', 'created_by');
   }

   public function updatedBy(){
   	return $this->belongsTo('App\User', 'updated_by');
   }

   public function detail()
   {
      return $this->hasMany('App\SalesDetail', 'sales');
   }

   static function filter($data)
   {
      //dd($data);
      $_order_type       = '';
      $_order_number     = '';
      $_start_at         = '';
      $_end_at           = '';
      $_range_at         = '';
      $_customer_name    = '';
      $_customer_contact = '';
      $_print            = '';

      if ($data['order_type'] != '') {
         $_order_type = " and order_type = '".$data['order_type']."'";
      }
      
      if ($data['order_number'] != '') {
         $_order_number = " and order_number = '".$data['order_number']."'";
      }

      if ($data['customer_name'] != '') {
         $_customer_name = " and customer_name = '".$data['customer_name']."'";;
      }

      if ($data['customer_contact'] != '') {
         $_customer_contact = " and customer_contact = '".$data['customer_contact']."'";
      }

      if ($data['print'] != '') {
         $_print = " and is_print = '".$data['print']."'";
      }

      if ($data['start_at'] != '' && $data['end_at'] == '') {
         $_start_at = " and order_date >= '".$data['start_at']."'";
      }

      if ($data['end_at'] != '' && $data['start_at'] == '') {
         $_end_at = " and order_date <= '".$data['end_at']."'";
      }

      if ($data['start_at'] != '' && $data['end_at'] != '') {
         $_range_at = " and order_date >= '".$data['start_at']."' and order_date <= '".$data['end_at']."'";
      }

      //dd($_print);
      $data = DB::SELECT("SELECT * FROM sales 
         WHERE is_published = '1'
         ".$_order_type."
         ".$_order_number."
         ".$_customer_name."
         ".$_customer_contact."
         ".$_start_at."
         ".$_end_at."
         ".$_range_at."
         ".$_print."
         ");
      //dd($data);
      return $data;
   }
}
